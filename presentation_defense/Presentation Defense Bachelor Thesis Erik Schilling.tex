\documentclass[14pt]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{smartdiagram}
\usesmartdiagramlibrary{additions}
\usepackage{listings}
\usepackage{pdfpcnotes}


\mode<presentation> {
	\setbeamertemplate{footline}{
	  \begin{beamercolorbox}[sep=-2ex]{author in head/foot}
	    \hfill
	    %\normalsize\textit{\insertsection}
	    \hfill
	    \llap{\large\insertpagenumber}
	    \hspace{5mm}
	    \vspace{5mm}
	  \end{beamercolorbox}
	}
	\setbeamertemplate{navigation symbols}{}
}

\title{Single step deployment from source code to OpenStack}

\author{Erik Schilling
\\
\href{mailto:ablu@mail.upb.de}{ablu@mail.upb.de}}
\date{\today}

\begin{document}
\smartdiagramset{
back arrow disabled=true,
module minimum width=2cm,
text width=2cm,
	}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Overview}
\tableofcontents
\end{frame}

\section{Motivation}

\begin{frame}
	\begin{center}
		\huge Motivation
	\end{center}

	\begin{itemize}
		\item cloud computing is getting very popular
		\item different level of service available:
	\end{itemize}

	\begin{center}
		\smartdiagram[descriptive diagram]{
			{IaaS, {full flexibility, requires sophisticated configuration}},
			{PaaS, {ready to use platform, only supports specific framework (like LAMP)}},
			{SaaS, {ready to use software, \\but nothing else}},
		}
	\end{center}
	\pnote{- simplicity increasing to the bottom, flexibility is increasing to the top}
	\pnote{- we want the flexibility of IaaS and the simplicity of SaaS}
\end{frame}

\begin{frame}
	\begin{center}
		\huge Motivation
	\end{center}

	If we want the full flexibility we are forced to use \textit{IaaS}. This means we need to:

	\begin{itemize}
		\item pick a base image
		\item install requirements to compile
		\item compile
		\item test
		\item install into target image
	\end{itemize}

	\pnote{- error prone procedure}
	\pnote{- time consuming}
\end{frame}

\section{Goal}

\begin{frame}
	\frametitle{Goal:}

	\begin{center}
		\vspace{-40pt}
		\LARGE Single step from code to deployed application
	\end{center}

	\begin{center}
		\scalebox{0.7}{
			\smartdiagram[flow diagram:horizontal]{Code, Compile, Test, Build Image, Deploy}
		}
	\end{center}

	\begin{itemize}
		\item Support for arbitrary languages
		\item Isolated build and testing environment
		\item Release (like) environment
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Example Use Case}
	\begin{itemize}
		\item Backend:\\
		Language: Java\\
		Build: Maven\\
		Service: Webservices providing API
		\item Frontend:\\
		Language: HTML, JS, ...\\
		Build: Gulp\\
		Service: Hosted files provided by a web server\\
		\item Database: pre-configured image
	\end{itemize}
\end{frame}

\section{Solution}

\begin{frame}
	\frametitle{Potential solutions}

	\begin{itemize}
		\item Standalone tool
		\begin{itemize}
			\item hard to keep maintained
		\end{itemize}

		\item Solum\footnote{\url{https://wiki.openstack.org/wiki/Solum}}
		\begin{itemize}
			\item extend Solum in order to support all use cases
			\item utilize Solum for the build step at least
		\end{itemize}

		\item HEAT plugin
		\begin{itemize}
			\item define build as part of a HOT file
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Solum}
	\begin{itemize}
		\item Docker based builds of applications are supported
		\item deployment of built applications
		\item integration with Git hosting via web hooks
	\end{itemize}

	\begin{center}
		\scalebox{1.0}{
			\smartdiagram[flow diagram:horizontal]{Code Change, Docker build, Deploy}
		}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Solum}
	\begin{itemize}
		\item defines file format for application description

			\begin{center}
				\scalebox{1.0}{
					\smartdiagram[flow diagram:horizontal]{Language pack, Appfile, Docker image}
				}
			\end{center}

		\item API allows easy use of implementation parts
		\item \textbf{suitable upstream project}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Build definition}

	\footnotesize
	\begin{lstlisting}
version: 1
name: wordpress
description: WordPress
languagepack: centos7
source:
  repository: https://github.com/Ablu/WordPress.git
  revision: master
trigger_actions:
- build
- deploy
ports:
- 80
...
	\end{lstlisting}
	\normalsize
\end{frame}

\begin{frame}[fragile]
	\frametitle{Build definition file}

	\footnotesize
	\begin{lstlisting}
#!/bin/bash

sudo yum install -y httpd mariadb-server \
	mariadb php php-mysql
sudo systemctl enable httpd
sudo systemctl enable mariadb

sudo systemctl start mariadb
echo "CREATE DATABASE wordpress" | sudo mysql
echo "GRANT ALL ON wordpress.*
	TO 'username'@'localhost'
	IDENTIFIED BY 'password'" | sudo mysql
echo "FLUSH PRIVILEGES" | sudo mysql

sudo cp -r * /var/www/html/
	\end{lstlisting}
	\normalsize
\end{frame}

\begin{frame}
	\frametitle{Solum Workflow}
	\begin{center}
		\scalebox{1.0}{
			\smartdiagram[flow diagram:vertical]{Create Language pack, Develop Application, Create Appfile, solum deploy $<$app$>$}
		}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Solum Architecture}

	\begin{figure}
	    \centering
	    \makebox[\textwidth]{
			\includegraphics[width=0.9\paperwidth]{figures/solum_architecture.png}
		}
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{VM Build Service}

	\begin{itemize}
		\item API can be reused
		\item only worker and deployer need to be exchanged
		\item implementation has to follow existing interface
	\end{itemize}

	\footnotesize
	\begin{lstlisting}
def launch_workflow(self, ctxt, build_id,
                    git_info, ports, name,
                    base_image_id, source_format,
                    image_format, assembly_id,
                    workflow, test_cmd,
                    run_cmd, du_id):
	\end{lstlisting}
	\normalsize
\end{frame}

\begin{frame}[fragile]
	\frametitle{Packer}

	\footnotesize
	\begin{lstlisting}
"builders": [
    {
        "type": "openstack",
        "image_name": "result_name",
        "source_image": <glance image id>,
        "flavor": "2"
    }
],
"provisioners": [
    {
        "type": "shell",
        "inline": [
            "sudo dnf install -y nginx"
        ]
    }
]
	\end{lstlisting}
	\normalsize
\end{frame}


\begin{frame}[fragile]
	\frametitle{Language Pack}

	\footnotesize
	\begin{lstlisting}
solum lp create centos7 glance://<image id>
	\end{lstlisting}
	\normalsize

	\begin{itemize}
		\item additional abstraction
		\item compatible to the Solum Docker workflow
		\item allows more sophisticated solutions in the future
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{HEAT plugin}

	\footnotesize
	\begin{lstlisting}
resources:
  image:
    type: OS::Solum::Build
    properties:
      app_name: share
  api_instance:
    type: OS::Nova::Server
    properties:
      image: { get_attr: [api_image, external_ref] }
      flavor: m1.small
	\end{lstlisting}
	\normalsize
\end{frame}

\begin{frame}
	\frametitle{HEAT plugin}
	\begin{itemize}
		\item multiple components can be combined
		\item easy integration into existing solutions
		\item does not require modification of existing resource plugins
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{HEAT plugin}

	\begin{figure}
	    \centering
	    \makebox[\textwidth]{
			\includegraphics[width=0.95\paperwidth]{figures/solum_heat_plugin_architecture.png}
		}
	\end{figure}
\end{frame}

\section{Conclusion}

\begin{frame}
	\frametitle{Conclusion}

	\begin{itemize}
		\item interesting to work together with an established team
		\item
	\end{itemize}
\end{frame}

\end{document}
