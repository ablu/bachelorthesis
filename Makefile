PROPOSAL_DOCUMENT_NAME=Proposal\ Bachelor\ Thesis\ Erik\ Schilling
PRESENTATION_DOCUMENT_NAME=Presentation\ Proposal\ Bachelor\ Thesis\ Erik\ Schilling
DEFENSE_PRESENTATION_DOCUMENT_NAME=Presentation\ Defense\ Bachelor\ Thesis\ Erik\ Schilling
THESIS_DOCUMENT_NAME=Bachelor\ Thesis\ Erik\ Schilling
THESIS_FIGURES=$(wildcard thesis/figures/*.dia)
DEFENSE_PRESENTATION_FIGURES=$(wildcard presentation_defense/figures/*.dia)

all: build/${PROPOSAL_DOCUMENT_NAME}.pdf build/${PRESENTATION_DOCUMENT_NAME}.pdf build/${THESIS_DOCUMENT_NAME}.pdf  build/${DEFENSE_PRESENTATION_DOCUMENT_NAME}.pdf

build/${PROPOSAL_DOCUMENT_NAME}.pdf: $(shell find proposal/ -iname '*.tex' -or -iname '*.bib' | sed 's/ /\\ /g')
	mkdir -p build && \
	cd proposal && \
	latexmk -interaction=nonstopmode -pdf ${PROPOSAL_DOCUMENT_NAME}.tex && \
	mv ${PROPOSAL_DOCUMENT_NAME}.pdf ../build

build/${THESIS_DOCUMENT_NAME}.pdf: thesis_figures $(shell find thesis/ -iname '*.tex' -or -iname '*.bib' -or -iname '*.json' | sed 's/ /\\ /g'; find thesis/listings/)
	mkdir -p build && \
	cd thesis && \
	latexmk -r ../peng.rc -interaction=nonstopmode -pdf ${THESIS_DOCUMENT_NAME}.tex && \
	cp ${THESIS_DOCUMENT_NAME}.pdf ../build


build/${PRESENTATION_DOCUMENT_NAME}.pdf: $(shell find presentation_proposal/ -iname '*.tex' | sed 's/ /\\ /g')
	mkdir -p build && \
	cd presentation_proposal && \
	latexmk -interaction=nonstopmode -pdf ${PRESENTATION_DOCUMENT_NAME}.tex && \
	latexmk -interaction=nonstopmode -pdf ${PRESENTATION_DOCUMENT_NAME}.tex && \
	mv ${PRESENTATION_DOCUMENT_NAME}.pdf ../build && \
	mv ${PRESENTATION_DOCUMENT_NAME}.pdfpc ../build


build/${DEFENSE_PRESENTATION_DOCUMENT_NAME}.pdf: defense_presentation_figures $(shell find thesis/ -iname '*.tex' -or -iname '*.bib' -or -iname '*.json' | sed 's/ /\\ /g'; find thesis/listings/)
	mkdir -p build && \
	cd presentation_defense && \
	latexmk -interaction=nonstopmode -pdf ${DEFENSE_PRESENTATION_DOCUMENT_NAME}.tex && \
	cp ${DEFENSE_PRESENTATION_DOCUMENT_NAME}.pdf ../build

thesis_figures: $(THESIS_FIGURES:.dia=.png)
defense_presentation_figures: $(DEFENSE_PRESENTATION_FIGURES:.dia=.png)

%.svg: %.dia
	dia "$<" -e "$@"

%.png: %.svg
	inkscape "$<" -j -C --export-png="$@"

clean:
	rm -rf build/
