\documentclass[14pt]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{smartdiagram}
\usesmartdiagramlibrary{additions}
\usepackage{listings}
\usepackage{pdfpcnotes}


\mode<presentation> {
	\setbeamertemplate{footline}{
	  \begin{beamercolorbox}[sep=-2ex]{author in head/foot}
	    \hfill
	    %\normalsize\textit{\insertsection}
	    \hfill
	    \llap{\large\insertpagenumber}
	    \hspace{5mm}
	    \vspace{5mm}
	  \end{beamercolorbox}
	}
	\setbeamertemplate{navigation symbols}{}
}

\title{Single step deployment from source code to OpenStack}

\author{Erik Schilling
\\
\href{mailto:ablu@mail.upb.de}{ablu@mail.upb.de}}
\date{\today}

\begin{document}
\smartdiagramset{back arrow disabled=true}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Overview}
\tableofcontents
\end{frame}

\section{Motivation}

\begin{frame}
	\begin{center}
		\huge Motivation
	\end{center}

	\begin{itemize}
		\item cloud computing is getting very popular
		\item different level of service available:
	\end{itemize}

	\begin{center}
		\smartdiagram[descriptive diagram]{
			{IaaS, {full flexibility, requires sophisticated configuration}},
			{PaaS, {ready to use platform, only supports specific framework (like LAMP)}},
			{SaaS, {ready to use software, \\but nothing else}},
		}
	\end{center}
	\pnote{- simplicity increasing to the bottom, flexibility is increasing to the top}
	\pnote{- we want the flexibility of IaaS and the simplicity of SaaS}
\end{frame}

\begin{frame}
	\begin{center}
		\huge Motivation
	\end{center}

	If we want the full flexibility we are forced to use \textit{IaaS}. This means we need to:

	\begin{itemize}
		\item pick a base image
		\item install requirements to compile
		\item compile
		\item test
		\item install into target image
		\item test again
		\item ...
	\end{itemize}

	\pnote{- error prone procedure}
	\pnote{- time consuming}
\end{frame}

\section{Goal}

\begin{frame}
	\frametitle{Goal:}

	\begin{center}
		\vspace{-40pt}
		\LARGE Single step from code to deployed application
	\end{center}

	\begin{center}
		\scalebox{0.7}{
			\smartdiagram[flow diagram:horizontal]{Code, Compile, Test, Build Image, Test, Deploy}
		}
	\end{center}

	\begin{itemize}
		\item Support for arbitrary languages
		\item Isolated build and testing environment
		\item Release (like) environment
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Goal}

	The solution should be able to:

	\begin{itemize}
		\item execute builds in an isolated environment
		\item transfer the build results to a release image
		\item configure the release image
		\item run tests on the release image
		\item make the image available for use within OpenStack
		\item allow to specify the interaction between images
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Example Use Case}
	\begin{itemize}
		\item Backend:\\
		Language: Java\\
		Build: Maven\\
		Service: Webservices providing API
		\item Frontend:\\
		Language: HTML, JS, ...\\
		Build: Gulp\\
		Service: Hosted files provided by a web server\\
		\item Database: pre-configured image
	\end{itemize}
\end{frame}

\section{Tasks}

\begin{frame}
	\frametitle{Tasks}

	\begin{itemize}
		\item Research possible invocation logic
		\item Implement build logic
		\begin{itemize}
			\item Build definition file
			\item Build
			\item Installation into target environment
			\item Providing the result image
		\end{itemize}
		\item Integration testing
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Research tooling / possible implementation}

	Some thoughts for now:

	\begin{itemize}
		\item Glance plugin
		\begin{itemize}
			\item generate images on request
		\end{itemize}
		\item HEAT plugin
		\begin{itemize}
			\item use repository URLs instead of image names and generate the images
		\end{itemize}
		\item Solum\footnote{\url{https://wiki.openstack.org/wiki/Solum}}
		\begin{itemize}
			\item extend Solum in order to support all use cases
			\item utilize Solum for the build step at least
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Glance plugin / extension}

	\begin{itemize}
		\item would be transparent to existing tooling
		\item how to deal with errors?
		\item are plugins possible?
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{HEAT plugin}

	\begin{itemize}
		\item orchestration would come for free
		\item would be restricted to HOT files
		\item (basic) plugin documentation is available\footnote{\url{https://wiki.openstack.org/wiki/Heat/Plugins}}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Solum}
	\begin{itemize}
		\item existing Project which can build images from Git
		\item API allows easy use of implementation parts
		\item could be a suitable upstream project to which the changes could be contributed
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Build logic -- Build definition file}
	\begin{columns}
	    \begin{column}{0.45\textwidth}
			\begin{itemize}
				\item build
				\begin{itemize}
					\item metadata
					\begin{itemize}
						\item base image
						\item ?
					\end{itemize}
					\item commands
				\end{itemize}
				\item release
				\begin{itemize}
					\item metadata
					\begin{itemize}
						\item base image
						\item files required in release
						\item ?
					\end{itemize}
					\item commands
				\end{itemize}
			\end{itemize}

			\pnote{- Additionally some info about instance size will be required}
		\end{column}
		\begin{column}{0.48\textwidth}
			\pause
			\small
			\begin{lstlisting}
				build:
				  baseimage: "devenv"
				  commands:
				    - make install \
				      destdir=/build
				release:
				  baseimage: "centos"
				  files:
				    - /build/
				  commands:
				    - # configure nginx
				    - # test site
				    - curl 127.0.0.1/
			\end{lstlisting}
		\end{column}
	\end{columns}

	\pnote{- only a concept file-format}
	\pnote{- for installation ansible, cloud-init, ..., could be evaluated. But would add more complexity}
	\pnote{- testing the release build might need to be done after a reboot / detached volumes}
\end{frame}

\begin{frame}
	\frametitle{Build logic -- Image interaction definition}

	If HOT files are taken as base those can do the job.

	\vspace{30pt}

	Otherwise a file format has to be developed which allows to express the interaction of the images. This would be quite a bit of work...

	\vspace{20pt}

	ideally HEAT should be reused..
\end{frame}

\begin{frame}
	\frametitle{Build logic -- Build}
	\begin{itemize}
		\item isolated, reproducible build environment
		\pnote{- it will have to happen in a vm}
		\item execution of a list of commands
		\item test execution is covered by simple commands
		\item exit code != 0 fails the build
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Build logic -- Providing the result image}
	\begin{itemize}
		\item transfer build results:
		\begin{itemize}
			\item use cinder volume
			\pnote{- cinder volumes will not leave leftovers on the system after detach}
			\item copy base image and patch it directly
			\pnote{- when patching, how to remove again? patch again?}
		\end{itemize}
		\item commands which initialize and test the application
		\item make the image available for Nova
		\begin{itemize}
			\item Glance upload
			\item snapshot
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Integration testing}

	\begin{itemize}
		\item set up testbed on physical machines
		\item test real world use cases
		\item figure out room for improvements
	\end{itemize}
	\pnote{- small improvements could maybe directly implemented, others could be useful for the conclusion of the thesis}
\end{frame}

\section{Work plan}

\begin{frame}
	\frametitle{Work plan}

	\begin{tabular}{l p{7cm}}
		\textbf{week 1-3} &
		getting familiar with OpenStack, evaluate implementation hooking points \\

		\textbf{week 4-5} &
		setup of development environment, evaluation of tooling \\

		\textbf{week 6-9} &
		build step implementation \\

		\textbf{week 10-12} &
		installation step implementation \\

		\textbf{week 13} &
		finishing of implementation / integration \\

		\textbf{week 14-16} &
		production like OpenStack install, testing of use cases \\

		\textbf{week 17+} &
		proof-reading, buffer \\
	\end{tabular}
\end{frame}

\section{Feedback}

\begin{frame}
	\frametitle{Questions?}

	\large Feedback and suggestions are welcome too...
	\pnote{- title suggestions?}
\end{frame}

\begin{frame}
	\frametitle{Titles?}
	\large
	\begin{itemize}
		\item Single step deployment from source code to OpenStack
		\item Simplifying deployment of source code to OpenStack
		\item Development of a single step solution for deploying source code to OpenStack
	\end{itemize}

\end{frame}

\end{document}
