CLIENT_NAME = 'solum'

class SolumClientPlugin(client_plugin.ClientPlugin):

    exceptions_module = exceptions

    service_types = [APPLICATION_DEPLOYMENT] = ['application_deployment']

    def _create(self, version=None):
        con = self.context
        endpoint_type = self._get_client_option(CLIENT_NAME, 'endpoint_type')
        args = {
            'endpoint': 'http://10.0.2.15:9777',
            'token': con.auth_token,
            'auth_url': con.auth_url,
            'tenant_name': 'demo',
        }
        return solum_client.Client(version if version else '1', **args)
