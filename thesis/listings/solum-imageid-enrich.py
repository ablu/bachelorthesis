# Make the build result public
if assembly.image_id:
    img = objects.registry.Image.get_by_id(self.context, assembly.image_id)
    wf.external_ref = img.external_ref
else:
    wf.external_ref = None
