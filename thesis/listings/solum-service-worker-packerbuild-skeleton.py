def _packer_build(ctxt, auth_info, git_clone_dir, base_image_id):
    packer_template = {
        "builders": [
            {
                "type": "openstack",

                "identity_endpoint": auth_info["auth_url"],
                "username": auth_info["username"],
                "password": auth_info["password"],
                "tenant_name": auth_info["tenant_name"],

                "source_image": base_image_id,
                "floating_ip_pool": cfg.CONF.worker.packer_floating_ip_pool,
                "flavor": cfg.CONF.worker.packer_default_flavor,

                "image_name": image_name,
            },
        ],
    }

    packer_template_file_fd, packer_template_file = tempfile.mkstemp()
    try:
        os.write(packer_template_file_fd,
                 json.dumps(packer_template, packer_template_file_fd))
        os.fsync(packer_template_file_fd)
        os.close(packer_template_file_fd)
        p = subprocess.Popen([cfg.CONF.worker.packer_bin, "build",
                             packer_template_file])
        p.wait()
    finally:
        os.remove(packer_template_file)
