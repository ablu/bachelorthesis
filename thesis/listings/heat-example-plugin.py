from heat.common.i18n import _
from heat.engine import resource
from heat.engine import attributes
from heat.engine import properties


class ImagePlugin(resource.Resource):
    """A resource which represents a generated image"""

    properties_schema = {
        "git": properties.Schema(
            properties.Schema.STRING,
            _('Git repository to build from.')
        ),
    }

    attributes_schema = {
        "value": attributes.Schema(
            _('The image which this plugin generates'),
            cache_mode=attributes.Schema.CACHE_NONE,
            type=attributes.Schema.STRING
        ),
    }

    def init(self):
        self.attributes_schema.update(self.base_attributes_schema)

    def handle_create(self):
        self.data_set('value', 'Fedora-Cloud-Base-23-20151030.x86_64', redact=True)

    def get_attribute(self, key, *path):
        if key == "value":
            return self.data().get("value")
        return None


def resource_mapping():
    return {
        'My::Image::Plugin': ImagePlugin
    }
