build_timeout = self.properties[self.BUILD_TIMEOUT]
try_until = time.time() + 60 * build_timeout
while time.time() < try_until:
    time.sleep(5)
    # Check if build was successful return and return in this case

raise exception.Error(_("Build of the Solum app %s timed out after %d seconds") % (app_name, build_timeout))
