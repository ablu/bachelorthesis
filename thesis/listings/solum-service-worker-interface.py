class Handler(object):
    def launch_workflow(self, ctxt, build_id, git_info,
                        ports, name, base_image_id,
                        source_format, image_format,
                        assembly_id, workflow, test_cmd,
                        run_cmd, du_id):
        pass

    def build(self, ctxt, build_id, source_uri, name,
              base_image_id, source_format,
              image_format, assembly_id,
              test_cmd, artifact_type=None):
        pass

    def unittest(self, ctxt, build_id, source_uri, name,
                 base_image_id, source_format,
                 image_format, assembly_id,
                 test_cmd):
        pass

    def build_lp(self, ctxt, image_id, source_info,
                 name, source_format, image_format,
                 artifact_type, lp_params):
        pass
