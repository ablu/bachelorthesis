def _process_output(cmdline):
    p = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    exit_code = None
    while exit_code is None:
        exit_code = p.poll()
        line = p.stdout.readline()
        if line:
            yield line

    # Read what is left on stdout
    while True:
        line = p.stdout.readline()
        if line:
            yield line
        else:
            break
    if exit_code != 0:
        raise Exception("cmdline %s failed" % cmdline)
