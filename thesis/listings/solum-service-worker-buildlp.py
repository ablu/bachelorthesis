def build_lp(self, ctxt, image_id, source_info, name, source_format,
             image_format, artifact_type, lp_params):
    LOG.debug("Building languagepack %s" % name)
    update_lp_status(ctxt, image_id, name, IMAGE_STATES.BUILDING)
    try:
        source_uri = source_info['source_url']
        url = urlparse(source_uri)
        if url.scheme == "glance":
            image = url.netloc
            verify_image_exists_in_glance(ctxt, image)
            update_lp_status(ctxt, image_id, name, IMAGE_STATES.READY, image)
        else:
            LOG.error("Unsupported source_uri %s" % source_uri)
            update_lp_status(ctxt, image_id, name, IMAGE_STATES.ERROR)
    except:
        update_lp_status(ctxt, image_id, name, IMAGE_STATES.ERROR)
        raise
