def handle_create(self):
    app = self.client().apps.find(
            name_or_id=self.properties[self.APP_NAME])

    workflow = WorkflowManager(self.client(), app_id=app.id).create(actions=["build"])
    id_ = workflow.id
    while True:
        time.sleep(5)
        workflow = WorkflowManager(self.client(), app_id=app.id).find(revision_or_id=id_)
        if workflow.status == 'BUILT':
            self.data_set(self.EXTERNAL_REF, workflow.external_ref, redact=True)
            return
