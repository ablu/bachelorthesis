Automating the process of getting from source code to a running application is not a new idea. Especially the DevOps trend demands for more automation during the deployment phase of an application. The core idea is to minimize any manual intervention in order to be able to bring a code change into a release system in a matter of minutes. The term continuous delivery or better continuous deployment is often used to describe the process of building, testing and deploying an application directly from source code. The number of different solutions for this task is overwhelming. In the following section we will outline a few solutions and tools which relate to the problem of deploying source code to the cloud.

\paragraph{Heroku:}
\label{par:Heroku}
Heroku\footnote{\url{https://www.heroku.com/}} is a commercial \ac{PaaS} provider. Before deploying an application to Heroku the user chooses a build pack. Heroku comes with a few official build packs\footnote{\url{https://elements.heroku.com/buildpacks}} but an user can also define his own. A build pack usually provides support for a specific language or framework. The Java build pack, for example, will come with all tools required to compile and run Java applications. After choosing the build pack, deploying an application happens by pushing source code to a special Git remote. So after doing a new commit in the source code repository one would run \texttt{git push heroku master} to push the latest code to Heroku. On the server this code is then built according to the build pack. For a Java application this could, for example, mean that a Maven\footnote{Maven is a popular build manager for Java projects: \url{https://maven.apache.org/}} build procedure is started. The built results are then packed into a so-called \emph{slug}. A \emph{slug} contains everything which is required to run the application (binaries, dependencies, ...). When the application is deployed, the server will then start a minimal execution environment based on the build pack. A metadata file from the application source code then tells the Heroku server how the application is executed. Internally Linux containers are used for execution, but since the concept of Heroku is to handle everything related to deployment the user usually does not have to care about the details of executing the applications.

While Heroku is proprietary software and does not offer to run their server part in a self-hosted environment, there also exist open source solutions which can be hosted in self-controlled environments like Cloud Foundry\footnote{\url{https://www.cloudfoundry.org/}} or Convox\footnote{\url{https://convox.com/}}. Both are inspired by the Heroku workflow.

A very popular open source alternative to Heroku is OpenShift. OpenShift utilizes the Kubernetes\footnote{\url{http://kubernetes.io/}} container orchestration software for running applications. These containers are a lightweight alternative to conventional virtual machines. Containers are sharing the kernel with the host operating system but run in an isolated scope which offers seperation from the host or other containers. While Kubernetes mainly handles the execution of containers, OpenShift is providing utilities around that for building and managing those containers. Otherwise the general usage is very similar to Heroku. An user creates a Git repository and pushes his code to it. OpenShift then automatically deploys the application. However, OpenShift allows a bit more flexibility, for example by also providing \ac{SSH} access to the launched applications. Unfortunately, like all the other alternatives named before, OpenShift only supports containers. \ac{VM} images are not supported.


\paragraph{Vagrant}
\label{par:Vagrant}
Vagrant is a tool for building \ac{VM} images. The most common use case is to generate reproducible development environments for applications which are not that easy to install directly on the developer's machine. OpenStack, for example, is making heavy use of Vagrant throughout all its subprojects. Installing all the required OpenStack components on the development machine would be a lot of work and exchanging parts of the components with different versions would be near to impossible to manage. So usually the developer uses Vagrant to launch a \ac{VM} and does all the development and testing within this environment. The specification about which base image to use for building, the amount of resources available to the \ac{VM} (RAM, disk space, CPU cores) and finally a script which does the setup is defined in a file called \emph{Vagrantfile}. One interesting feature of Vagrant is, that Vagrant automatically synchronizes the working directory it was started with to a directory within the \ac{VM}. This means that it is possible to create a \emph{Vagrantfile} in the root directory of the application source code. Now the launched \ac{VM} has full access to this code and can run steps like compilation and testing. It is easy to see that there are quite a few similarities to the workflow we have in mind for our OpenStack solution. While Vagrant officially only supports providers for VirtualBox, Hyper-V, Docker and VMware, there are a couple of community supported providers. Among these there also is an OpenStack provider\footnote{\url{https://github.com/ggiamarchi/vagrant-openstack-provider}}. However, the main use case of Vagrant is still providing development environments. This is an important difference to the use case that we have planned for OpenStack. For our use cases we want to build the applications once and then deploy them multiple times (or possibly scale them using HEAT). But with Vagrant we usually build every new instance entirely from scratch. Another smaller drawback is that Vagrant requires a specific image definition format. So for using an OpenStack image, which is stored in Glance already, we would need to create a small metadata file\footnote{\url{https://github.com/ggiamarchi/vagrant-openstack-provider\#box-format}} which registers the image as a \emph{Box} for Vagrant. This \emph{Box} can then be used as a base image in a \emph{Vagrantfile}.


\paragraph{Murano:}
\label{par:Murano}
Murano\footnote{\url{https://wiki.openstack.org/wiki/Murano}} does not really have a focus on building applications from code. It mainly tries to make deployment of existing applications with OpenStack really easy. Murano first requires the creation of an application package. This works by creating a series of configuration files which define how the application is deployed\footnote{\url{http://docs.openstack.org/developer/murano/appdev-guide/step\_by\_step.html}}. After doing this once, we can load this package into an OpenStack instance which has Murano installed. Now launching a new environment which contains this application package can be done with only a couple of clicks. Of course it would be possible to download source code, build that code and deploy it as part of the deployment step. However, the build would have to happen on every deploy. Furthermore, it is questionable how compilation errors during the build could be handled.


\paragraph{IM}
\label{par:IM}
IM\footnote{\url{https://imdocs.readthedocs.io/en/latest/index.html}} is another approach to simplify the deployment of cloud infrastructure. The tool was developed by the research group Grid and High Performance Computing from the Universitat Politècnica de València. The design of the software is explained in \cite{im_design_paper}. The team developed the \ac{RADL}\footnote{\url{https://imdocs.readthedocs.io/en/latest/radl.html}} for describing cloud resources. This language is independent from any specific cloud implementations. The solution can handle deployments to OpenStack, OpenNebula, OCCI, EC2 and libvirt. Due to a lack of a standard protocol for consuming cloud services this was solved using a plugin structure. The approach can be compared to the OpenStack specific HEAT service. But, just like HEAT, IM does not provide functionality for building application images from source code.


\paragraph{libguestfs:}
\label{par:libguestfs}

libguestfs\footnote{\url{http://libguestfs.org/}} is a collection of tools for working with \ac{VM} images. As the name suggests the project is mainly providing libraries. But there also is a series of command line programs available. With these tools it is possible to inspect or modify files within images directly.

More sophisticated tools like virt-customize can even run entire scripts inside an image. For security reasons this happens in a tiny \ac{VM} which mounts the image file and runs the script within a chroot\footnote{chroot is a tool to set the root directory of a command to a different folder. If this folder contains an operating system installation (like a \ac{VM} image) it is possible to run commands as if they were executed within this system directly. More information can be found at \cite{chroot_manpage}.}. An explanation why mounting disk images directly is a bad idea can be found at \cite{mount_security}. The core problem is that mounting a file system executes code of the relevant file system format handler of the kernel. Due to the large number of supported file sytem formats in the Linux kernel, this would allow exploiting of weaknesses in unpopular or obsolete file system implementations.

The following example installs nginx into a Fedora image:

\begin{lstlisting}[caption=virt-customize example which installs nginx into a Fedora Cloud image]
    virt-customize -a fedora-nginx.qcow2 --network --run-command "dnf install -y nginx" -v -x
\end{lstlisting}

\texttt{fedora-nginx.qcow2} initially is a clean Fedora Cloud image\footnote{\url{https://getfedora.org/de/cloud/download/}} and is then modified to have the nginx package installed. So virt-customize modifies the image file in-place.

Unfortunately virt-customize does not allow live inspection of command execution. The output of the commands executed in the image are redirected to a file within the image. But since the writes are not flushed before the execution ends one cannot use a second process to watch the file. This makes it very hard to debug failing command executions.


\paragraph{diskimage-builder:}
virt-customize and the other tools from the libguestfs project depend on running certain tasks within virtualized environments. While this provides abstraction and security it comes with a speed penalty. diskimage-builder\footnote{\url{http://docs.openstack.org/developer/diskimage-builder/}} does not rely on any virtualization. It does all the building natively on the host system in chroots. diskimage-builder was developed by the OpenStack team in order to make it easier to deploy OpenStack itself in cloud environments. For example, it is used to build images within the OpenStack continuous integration testing infrastructure. Unfortunately the project only allows to build images completely from scratch. Reusing existing images is not supported. In my experiments I also was unable to test some of the example command lines listed in the documentation. For example creating Ubuntu images failed with errors while creating the boot loader. Even after trying to consult the \ac{IRC} channel for support I was unable to generate a working image. Also the restriction to rebuild repetitively makes this tool not really suitable for a flexible solution to build arbitrary virtual machine images.

\paragraph{Packer:}
Packer\footnote{\url{https://www.packer.io/}} is trying a different approach than virt-customize or diskimage-builder. Rather than trying to mimic a virtual environment with minimal appliances or chroots, Packer starts the \ac{VM} image directly. A large number of builder plugins are available. Most interestingly there also is an OpenStack plugin. This is a big advantage, since it allows to run commands in an environment which equals to the environment during production. This could especially be useful for verifying built images. While building could happen in chrooted environments, testing should happen in an environment which is as close to production as possible. Generally Packer is comparable with Vagrant. However, Packer is a lot more light-weight and only handles image building. Also Packer does not require a specific image format to work with. Any OpenStack image format can be handled without prior registration.

The build is described using a \ac{JSON} file. An example configuration with OpenStack as builder plugin is shown in Listing~\ref{lst:packer-nginx-fedora-openstack-example}. The most basic sections in a Packer file are the \texttt{builders} and \texttt{provisioners} sections. The \texttt{builders} section specifies the type of the builder and metadata like the target image name, source image ID and the flavor of the machine to start. The \texttt{ssh\_username} section is required here since the Fedora cloud images only allow the login using this specific user. The \texttt{provisioners} section specifies how to provision the virtual machine. In this case we run a simple shell command in order to install nginx.

\lstinputlisting[caption={A Packer example file for installing nginx in an OpenStack provided Fedora image.},label=lst:packer-nginx-fedora-openstack-example,float]{listings/packer-fedora-nginx.json}

Packer then creates a temporary \ac{SSH} key, starts an instance of the specified \texttt{source\_image} and then executes the commands using \ac{SSH}. After that the instance is powered down and a snapshot is taken. In the end the temporary instance and \ac{SSH} key are destroyed and the only remaining part is the snapshot which is available in Glance as a base for future instances.


\paragraph{Solum:}
\label{par:Solum}

Solum\footnote{\url{https://wiki.openstack.org/wiki/Solum}} describes itself as ``An OpenStack project designed to make cloud services easier to consume and integrate into your application development process'' \cite{solum_summary}. The target of the project is very similar to the target of this thesis. It already allows to describe the build process of an application in a \ac{YAML} file and executes deployments according to this file. But, it only does this for Docker images. \ac{VM} images are currently not supported as build targets. However, I contacted the upstream developers on their \ac{IRC} channel and figured out that they would be interested into having support for \ac{VM} images too. This is especially interesting, since it would be highly desirable to push the code written during this thesis to an existing project.

Just like OpenShift, Solum was also inspired by the Heroku workflow. So the first step is to register a languagepack. The languagepack is equivalent to a Heroku buildpack. It defines how applications are compiled and later executed. In a Dockerfile build workflow this languagepack is the base image of the actual application image. After a languagepack is registered an application can be added.

An example app file from the Solum quick start guide \cite{solum_getting_started_guide} is shown in Listing~\ref{lst:solum-quickstart-appfile-example}. The file defines metadata like the name and description. Since the example app is a small webservice, which is implemented in Python, the languagepack is set to \texttt{python}. The repository and revision tell Solum where the source is located. The workflow\_config and trigger\_actions sections specify which commands are executed during the build and wether a deployment is triggered by default. Finally, the ports section specifies the ports which are accessible in the target instance.

\begin{lstlisting}[breaklines,float,label=lst:solum-quickstart-appfile-example,caption={A CherryPy application example from the Solum quick start guide \cite{solum_getting_started_guide}.}]
version: 1
name: cherrypy
description: python web app
languagepack: python
source:
  repository: https://github.com/rackspace-solum-samples/ solum-python-sample-app.git
  revision: master
workflow_config:
  test_cmd: ./unit_tests.sh
  run_cmd: python app.py
trigger_actions:
 - unittest
 - build
 - deploy
ports:
 - 80
\end{lstlisting}

After the application is added it is possible to deploy it. This works by running \texttt{solum app deploy cherrypy}. This will trigger a Git clone and start the build process. After the build is done the application will be deployed. The deployment happens as part of a HEAT stack. This is mainly required for doing advanced management tasks like scaling, for example. While this is the default workflow using the Solum commandline application, there also exists an user interface which integrates into the OpenStack dashboard. For integration with other applications there also is an \ac{API} available.

\paragraph{Conclusion:}
\label{par:Conclusion}
In this chapter an overview over multiple projects related to automating the process of getting from code to a running cloud instance was given. The tools are not all directly comparable since they solve different aspects of deploying code to a cloud system.

One group of presented applications are applications which build and deploy containers from source code. Table \ref{tab:container_paas_comparsion} is short comparison between those. While there are multiple solutions for container based deployments there do not seem to be complete solutions for building and deploying \ac{VM} images. This is interesting since the container technology is actually newer than conventional virtualization techniques. One potential reason might be that, since the technology is new, the tools around this technology are also relatively new. So these tools were designed at a time where continuous integration and delivery are a trending topic. Another interesting point to notice is that most presented projects take integration with \ac{SCM} systems serious. Apart from CloudFoundry all presented solutions can be configured to listen on Git hooks which can trigger builds and deployments of the software. This allows the developer to run a \texttt{git push} for triggering a new deployment of the software. CloudFoundry cannot do this without further work since it does not handle the building of applications. Here the application first needs to be build locally in order to be deployed.

\begin{table}
    \centering
    \begin{tabularx}{\textwidth}{|P|P|P|P|P|}
        \hline
        \textbf{Software} & \textbf{Source code} & \textbf{Hosting} & \textbf{Deployment} & \textbf{Supported source formats} \\
        \hline
        Heroku & not available & self hosting not possible & container based deployment (proprietary) & Git \\
        \hline
        OpenShift & open source & self hosting possible & Kubernetes for deployment & Git \\
        \hline
        CloudFoundry & open source & self hosting possible & Diego\footnote{\url{http://docs.cloudfoundry.org/concepts/how-applications-are-staged.html}} & binaries only \\
        \hline
        Convox & open source & private hosting on AWS & Docker & any, source code directory is uploaded \\
        \hline
        Solum & open source & self-hosted (currently no public hosting available) & Docker, Docker in \ac{VM} & Git \\
        \hline
    \end{tabularx}
    \caption{Comparison of container based \ac{PaaS} solutions. Criteria for the comparison are the availability of source code, available hosting options, the deployment technology and the supported source code formats}
    \label{tab:container_paas_comparsion}
\end{table}

Another group represents \ac{VM} image building tools. All this tools are capable of taking a series of commands and execute them in the context of an operating system image. However the way the tools approach this goal differs significantly. While diskimage-builder can do the whole build in a chrooted environment, Packer and Vagrant boot complete virtual machines in order to do the building. Both solutions have their pros and cons. While diskimage-builder can profit of the speed of building in a chroot, Vagrant and Packer allow the full flexibility of a virtualized environment. The fully virtualized environment allows a better seperation from the host environment regarding resource usage, but is considerably slower. Table \ref{tab:image_building_tool_comparsion} gives a short comparsion of the tools. Since diskimage-builder only supports building images from scratch and virt-customize does not allow inspection of the build logs these two options have a big disadvantage. Additionally it is very desirable to be able to run commands in a release like environment. So the two preferred tools are Vagrant and Packer. While Packer supports a larger number of builders we actually only need the OpenStack builder. Vagrant does not come with such a provider by default, but there is a community plugin for that. Vagrant comes with additional utilities like connecting to a running \ac{VM} via \ac{SSH} or a sophisticated snapshot management utility. However, we only really need to take a single snapshot for persisting the final build result. So both tools are more or less on pair feature wise for the desired use case. But since Vagrant needs a special registration of virtual machines as Boxes Packer seems to be the tool which is easier to consume in an automated process.

\begin{table}
    \centering
    \begin{tabularx}{\textwidth}{|P|P|P|P|}
        \hline
        \textbf{Tool} & \textbf{Officially supported hosts} & \textbf{Supported guests} & \textbf{Building environment} \\
        \hline
        diskimage-builder & Selected linux distributions\footnote{Recent versions of CentOS, Debian, Fedora, RHEL, Ubuntu and Gentoo are supported: \url{http://docs.openstack.org/developer/diskimage-builder/user_guide/supported_distros.html}} & Selected linux distributions\footnote{More or less the same distributions as host distributions are supported, additionally Ubuntu 12.04 is supported: \url{http://docs.openstack.org/developer/diskimage-builder/user_guide/supported_distros.html}} & chroot on host \\
        \hline
        virt-customize & Linux & Linux, Windows & chroot in a minimal appliance \\
        \hline
        Vagrant & Linux, Windows, OS X & Supported guest systems depend on the used provider. Requires repackaging as \textit{Box}. & default: VirtualBox, Hyper-V, Docker. Further providers are available from the community. \\
        \hline
        Packer & Linux, Windows, OS X, FreeBSD, OpenBSD & Supported guest systems depend on the used provider. & default builders: Amazon EC2, Azure, DigitalOcean, Docker, Google Compute Engine, OpenStack, Parallels (Mac), QEMU, VirtualBox, VMware. Custom builders are possible. \\
        \hline
    \end{tabularx}
    \caption{Comparison of tools to build or customize \ac{VM} images. Criteria are the supported host and guest systems and the supported build providers.}
    \label{tab:image_building_tool_comparsion}
\end{table}
