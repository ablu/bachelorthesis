\section{Development}
\label{sec:Development}
After we took a look at the goals, evaluated related projects and defined detailed development steps we are now ready to start implementing the solution. In this chapter we will first give a more detailed overview about the process of proposing a new change to Solum. We will take a more detailed look at the different Solum components and decide about a place where the build logic will be developed. After that, we will start with the implementation of the individual subtasks which we identified earlier.

All OpenStack projects are developed in Git repositories. So the process of contributing changes is to clone the repository, commit the changes and ask for a merge. The Solum documentation has a list of the required steps available at \cite{solum_contributing_guide}. Apart from the default Git workflow there are a few things to consider. First, due to copyright and patent reasons OpenStack only accepts code of developers who signed the \ac{CLA}. For this a Launchpad account is required. Launchpad is the platform which is used for tracking issues and feature requests for OpenStack projects. Once a Launchpad account is available, it is possible to become a member of the OpenStack foundation\footnote{\url{https://www.openstack.org/join/}} and sign the \ac{CLA}. For submitting and reviewing new commits to OpenStack project the Gerrit Code Review\footnote{\url{https://www.gerritcodereview.com/}} system is used. For easier usage the OpenStack team developed a small utility program \texttt{git-review}\footnote{\url{https://github.com/openstack-infra/git-review}} which can handle creation and updates of review requests. Once a review request is created other developer are able to review the change. Additionally to the manual review there also is a number of automatic checks which are executed based on the newly updated code. These checks, for example, execute unit tests or run certain code style checks. If one of these checks fails merging is blocked until the issue is resolved. So for a successful merge a review and a succeeding build is required.

After this short introduction to the process of introducing a change to Solum we will take a look at the general architecture of Solum. There are six different services which together form the Solum application. Figure~\ref{fig:solum-architecture} illustrates the relation between those components.


\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.4]{figures/solum_architecture.png}
  \caption{Illustration of the Solum architecture. The clients send their requests to the \ac{API} service. Internally a message queue is then used for distributing \ac{API} requests to the specialized services.}
  \label{fig:solum-architecture}
\end{figure}

\begin{enumerate}
    \item \textbf{api:}

    This is the \ac{API} server. It collects requests from clients and cares about issuing the execution. After receiving a request it enriches the received data with further context data from the database and forwards it to one of the other services. This happens by enqueuing the command into the OpenStack message queue server (usually RabbitMQ). Further work is then done by other services.

    \item \textbf{worker:}

    The worker does all the image building. It receives its commands from the message queue. It then clones the source repository, runs the build steps and persists the resulting image. If a deployment is required it triggers this by sending a command over the message queue to the deployer.

    \item \textbf{deployer:}

    As the name suggests, the deployer is caring about the deployment of built applications. This works by launching or updating HEAT stacks. It also handles the deletion and scaling of instances.

    \item \textbf{conductor:}

    Since the Solum team originally borrowed their architecture from Glance they also introduced a separate service for handling database access. The plan was that all database access is routed through the conductor service. This however lead to some additional complexity during the implementation of the other services. So this approach was discontinued and a lot of the new code directly accesses the database without using the conductor.

    \item \textbf{python-solumclient:}

    There also is a client library available for easier consumption of the \ac{API}. This takes the effort of implementing the \ac{API} invocations from the developer if Python is used. The library also comes with a command line tool for using Solum. While the previous services are developed within the same repository, this library is located in a separate repository\footnote{\url{https://github.com/openstack/python-solumclient}}.

    \item \textbf{solum-dashboard:}

    The OpenStack services usually come with a web based dashboard. Solum is no exception here. The dashboard is an alternative to the commandline tool for working with Solum. Like the command line tool this service is developed separately from the other services\footnote{\url{https://github.com/openstack/solum-dashboard}}.
\end{enumerate}

This separation of concerns is really useful. Since the image building process is encapsulated in the worker service we can work in a relatively self contained area of the code. As long as we stay compatible to the \ac{API} of the existing worker service we can exchange the entire service with our own without needing to modify any other of the services. Even better, the architecture of the worker service already allows exchanging of the worker handler. The logic which reads the commands from the message queue is separated from the actual build logic. The configuration option \texttt{handler} of the \texttt{[worker]} section specifies which handler is used for building. The same is possible for the deployer service. So a good way to implement the \ac{VM} building logic is to write such a new handler.

For the worker service a handler has to follow the interface shown in Listing~\ref{lst:worker-service-handler}. After developing a handler which implements this we can register it in the \texttt{main} function of the worker service as shown in Listing~\ref{lst:worker-service-handler-registration}. With this code skeleton we are now ready to start developing.

\lstinputlisting[language=Python,caption={The interface of the Solum worker service handler. The \texttt{unittest} and \texttt{build} functions actually only exist for compatibility reasons. The most important function is \texttt{launch\_workflow}. This function takes a parameter \texttt{workflow} as input which is an array with the actions to execute. More detail is given on page \pageref{par:meta-data-collection-paragraph}.}, label=lst:worker-service-handler]{listings/solum-service-worker-interface.py}

\begin{lstlisting}[caption={The handler registration in the \texttt{main} function of the worker service. The keys here are possible to set in the configuration}, label=lst:worker-service-handler-registration]
        handlers = {
            'noop': noop_handler.Handler,
            'default': default_handler.Handler,
            'shell': shell_handler.Handler,
            'vm': vm_handler.Handler, # <-- the new handler
        }
\end{lstlisting}



\input{03-implementation/04-development/01-build}
\input{03-implementation/04-development/02-heat}
\input{03-implementation/04-development/03-separate_result_image}
\input{03-implementation/04-development/04-integration_tests}
