\section{Development plan}
After setting up a development environment, we can now start planning the actual development.

In Section~\ref{sec:Evaluation of implementation point} we came to the conclusion that we will extend Solum in order to support our desired use cases. In this section we will specify this plan in a greater detail. First we will split the main goal from Section~\ref{sec:Goal} into multiple subtasks and then elaborate how we can solve this individual tasks.

We already figured out, that we can simplify our final goal down to building a single \ac{VM} since any orchestration can be done by a HEAT plugin. What is left is that we want to be able to build software in a clean environment, collect the build result, install the build result into a target image and then optionally run tests against this resulting image. This enumeration of tasks can directly serve as a base for splitting the main goal into multiple subgoals. Hence we will define our subgoals as the following:

\begin{enumerate}
    \item \textbf{Building of the software}

    For building an application image, an isolated build environment has to be provided. Subsequently the source code has to be transferred to it. A series of shell commands can then build, test and install the application. The resulting image is then saved for later use.

    \item \textbf{HEAT plugin for orchestration}

    As soon we are able to build and deploy a standalone \ac{VM} we can develop a HEAT plugin based on the proof of concept demonstrated in Listing~\ref{lst:heat-example-plugin}.

    \item \textbf{Installation of the software into the result image}

    As a first step for being able to install the software into an environment different from the build environment we need to figure out a way to collect the build result from the builder and then transfer that files into the target image. The solution itself is not really useful on its own but it is essential for the next step.

    After the build results arrived in the installation base image it should be easy to run the steps necessary to run a series of bash commands in order to install them. It should be the same logic as already required for the build step.

    \item \textbf{Integration testing with the result image}

    This step is largely independent from the other steps. In general we need to be able to start the resulting image from the previous step and, again, run a series of bash commands in that environment. These commands could then test the functionality of the final image. In order to be able to provision the necessary test data, accessing files from the source code repository is required again. However we already should have solved this issue during the earlier steps, so this should not be an issue at this point.
\end{enumerate}

The tasks should be completed in this strict order. If for some reason a certain task unexpectedly turns out to be harder or unsolvable within the given time constraints at least the previous tasks can still deliver a working result for a part of the total goal. Otherwise we might end up with solutions for certain problems, but will not be able to put these bits together to an useful solution. Especially the last two subgoals are nice to have, but not required for the solution to be usable. For each subtask we will initially only do the most basic steps which are required for the described functionality. Further solutions for optional tasks can then be developed after the main work is done if the time allows it.

We already evaluated Solum for being the best choice for contributing our changes. So in this section we have to plan the integration with the Solum architecture. Otherwise we will outline the problems which are required to be solved, but not to determine all the implementation decisions at this point. This will be done in the actual development sections later.

In the following subsections we will elaborate the outlined steps in further detail in order to get an overview about the work required to do.

\subsection{Building of the software}
\label{sub:Building of the software}

Getting from the source code to a result image which contains the compiled software arguably is the largest subtask. This is mostly the case because it also includes creating all the base technology which can then easily be reused in the following steps.

For this first subtask we will implement the building procedure of the software. We will start a \ac{VM} for building and then save the resulting state for deploying. This means that we will not implement a solution which allows to create a result image which is independent from the building environment within this step.

For being able to start a \ac{VM} instance we will require some meta data. This will include information like the base image and size constraints of the \ac{VM}. The existing Docker image building solution of Solum currently does not collect or require such information. It only required the user to specify a language pack for each application (which would then get injected as the \texttt{FROM:} part of the resulting temporary Dockerfile). So we will need to find a way to extend or modify the language pack system to also be able express \ac{VM} metadata.

The next step is to get the source code into the \ac{VM} instance. Ideally we should also be able to remove the source code after building. Otherwise the result image would still contain it. This might not be desirable for the result image size and maybe also due to confidential issues (if the source code is private). There are many options how the source code could be provided. The most common source code distribution formats are most likely \ac{SCM} systems, compressed archives or event a simple folder structure. Solum currently only supports the popular \ac{SCM} Git\footnote{\url{https://git-scm.com/}}. So for the first solution we will focus on building code from a Git repository.

After the code arrived in the \ac{VM} we will now need to run the build, test and installation commands in that environment. This means that we need to be able to register a series of shell commands for execution. After the execution we need to be able to tell whether the execution finished successfully and, for debugging purposes, be able to collect the output of the executed commands.

For being able to deploy the result without always needing to redo all the previous building steps we need to be able save the state of the \ac{VM} after building. This saved state then needs to be made available to Glance in order to be able to start fresh \ac{VM} instances from this base image.

Finally, since we want to submit this change to Solum we will also need to implement a deployer which can directly launch built images.

Summarized the steps for solving this subtask are: Collect metadata information about build environment, transfer the source code into a \ac{VM} instance, run build commands and collect the execution results, save the resulting state for later deployments and if required send a deployment request to the deployer.

\subsection{HEAT plugin for orchestration}
\label{sub:HEAT plugin for orchestration}

For implementing the HEAT orchestration we will need to extend the plugin demonstrated in Listing~\ref{lst:heat-example-plugin} to trigger a Solum build. For this we will enrich the plugin by using the Solum \ac{API}.

For being able to return a Glance image we need to be able to read the build result from the \ac{API} return value. During the implementation of the previous task it turned out that Solum currently does not store the build result in a way that the invoker can determinate it. This means that we will need to add storing of a reference to the build result somewhere in the deployment metadata.

The other development should be straight forward. The Solum Python library should allow the execution of all the tasks we need and since the HEAT bug which blocked the implementation of the plugin got fixed it should be possible to implement the plugin.

\subsection{Installation of the software into the result image}

For being able to separate the release image environment from the build environment we need to acquire the build result after building. This build result will need to be uploaded to the final install environment in a similar fashion as the source code is uploaded during the build step. After that a series of commands will need to be executed to install the build result into the image. The resulting image needs to be made available to Glance just like the build result image.

Overall it should be possible to reuse a lot of the logic from the previous tasks. But it will be required to develop a way to download the build result and extend the Solum app file format for specifying the release image and the commands which will install the build result.

\subsection{Integration testing the result image}
\label{sub:Integration testing the result image}


Especially when generating a result image which is independent of the build environment it is useful to be able to run integration tests to verify the result image. When doing the build and install in a single step it would be possible to run the integration tests as one of the last steps of the build. The integration tests would have access to the source code here and can access the test code and test data. In a separated result image only access to the build results would be available.

Thus the integration testing solution would need to launch the result image with the original source and then execute a series of commands. With the previous tasks already being solved, it should be possible to reuse the concept for uploading code and running commands to achieve this. Still, the app file format will require an extension to contain information about the test commands.
