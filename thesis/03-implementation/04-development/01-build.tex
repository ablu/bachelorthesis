\subsection{Building the software}
With this new handler, the subtasks defined in Section~\ref{sub:Building of the software} can now be implemented.

\paragraph{Collect metadata information about build environment:}
\label{par:meta-data-collection-paragraph}
Before we can start building we need to understand the information which is provided by the worker interface. Since the \texttt{build} and \texttt{unittest} functions only exist for compatibility reason we will focus on the \texttt{launch\_workflow} function. This function has the following parameters:

\begin{itemize}
    \item \texttt{ctxt}

    \texttt{ctxt} is an abbreviation for context. In this case the context of the service call. It contains information about the request and callee. It, for example, contains authentication tokens which allow authorization to other OpenStack services as the calling user. It also contains request metadata like an unique request ID.

    \item \texttt{build\_id}

    The \texttt{build\_id} is an unique integer identifying the build. The ID relates to a row in the \texttt{Image} database table and can be used to update the build status or other properties of the \texttt{Image} table.

    \item \texttt{git\_info}

    \texttt{git\_info} contains information about the Git source code repository. The main information it contains of course are the Git clone URL and revision identifier. But it also can include tokens or keys for accessing non public Git hostings.

    \item \texttt{ports}

    The \texttt{ports} parameter is an integer array which lists the ports which should be accessible in the running instance.

    \item \texttt{name}

    The name of the application to build.

    \item \texttt{base\_image\_id}

    \texttt{base\_image\_id} might be a bit misleading. Unlike the name might suggest, the parameter contains the language pack name. We will see later how this has to be interpreted.

    \item \texttt{source\_format}

    \texttt{source\_format} allowed to specify different building mechanisms for the Docker handler in the past. However it only seems to exist for historical reasons now since the \ac{API} always invokes the handler with \texttt{source\_format = 'solum'}.

    \item \texttt{image\_format}

    This is the \texttt{image\_format} as specified in the Solum configuration. The Docker builder, for example, uses this to choose among different target image formats.

    \item \texttt{assembly\_id}

    This is a reference to the built assembly in the database. An assembly is an instance of an application. While the application defines the metadata, the assembly contains information about a specific deployed instance of this application.

    \item \texttt{workflow}

    The \texttt{workflow} parameter contains information about what needs to be done with the application. This parameter is a string array which contains a list of commands to execute. Usually it contains the steps \texttt{build}, \texttt{unittest} and \texttt{deploy}. But it is also possible to, for example, only deploy an existing build or to build without unittesting.

    \item \texttt{test\_cmd}

    The command to execute for testing the software.

    \item \texttt{run\_cmd}

    The command to execute for running the software. For \ac{VM} images this will not be that important since we will register applications at the init system of the \ac{VM} for starting it on boot.

    \item \texttt{du\_id}

    The last parameter is the \texttt{du\_id}. \texttt{du} is an abbrevation for distribution unit. This parameter is used when an already built application should be deployed. In this case \texttt{workflow} only contains an \texttt{deploy} item and the \texttt{du\_id} is referencing an existing distribution unit.

\end{itemize}

All these parameters combined give a lot information which is required for building an image. Since Solum internally already has all this information available we do not need to implement parsing of the build description files on our own. We now know from where to get the source code and what to run in the final image. However we still need some information about the build environment. Mainly we still lack information about the Glance base image. For the Docker builds this works by building a Docker base image and then registering that image as languagepack. Then, for building, this image is used as base image by referencing it as \texttt{FROM: <languagepack>} in a Dockerfile. Since there is no equivalent to this Dockerfile mechanism, this cannot work the same way for \ac{VM} images.

In a Dockerfile one only has to define a series of shell commands. That works since it is possible to rely on the Docker Hub for getting the base images of a language pack here. For \ac{VM} images we do not have such a central place for acquiring base images. \ac{VM} images are usually provided on webservers of the individual vendors only. So while defining a language pack for the Solum Docker builds only required a Dockerfile and a build.sh script which handles the build process for applications, we need to rethink this approach for \ac{VM} images. At this point the most simple solution is to make a language pack a reference to a Glance image. Once we have a Glance image we can use that to start a \ac{VM} with this base image in order to build other images. It would even be possible to refactor Solum and accept any Glance image as \texttt{base\_image\_id}, however it still makes sense to keep the additional abstraction of the language pack. If we, for example, later want to introduce features like automatic downloading of the base image or allow to enrich the base image with pre-installed software, we can do this during the language pack creation step. But before we can start the implementation of the build step we need to implement the creation of the language pack.

Since we will only save a reference to a Glance image in the language pack, there is not a lot logic to develop for this step. However, we again need to see how we integrate this into the Solum architecture. For this we will first analyze how the process currently works for Docker based language packs. The starting point here is usually the invocation from the command line application. This looks like: \texttt{solum lp create <name> <git url>}. We pass a name and a Git URL to the \ac{API}. The \ac{API} runs a few checks like validating the name and verifying that the name is unique and forwards the creation request to the worker \ac{API}. Here it is received as a call to the \texttt{build\_lp} method of the registered handler. The Docker handler of the worker then clones the repository and builds the Dockerfile. The resulting image is then registered in the database. Overall we can reuse this workflow mostly. However we do not want to build the language pack from a Git repository at this point. We only really want to pass in an ID to a Glance image. During a discussion with the Solum team an idea came up to interpret the source URL in a broader term. We decided to allow to use arbitrary URIs like \texttt{glance://<image id>} as source URI. This solution has the nice effect, that it can be reused to add support for automatically downloading images for the use in language pack. For that case one could pass in \texttt{\url{http://cloud.centos.org/centos/7/images/CentOS-7-x86\_64-GenericCloud.qcow2}} as a source URI. The handler could then detect that it is an an image on the web and can download it to Glance. Adapting the client and the \ac{API} to handle arbitrary URIs instead of Git only was relatively easy. The variables only had to be renamed and a few checks had to be made a bit less restrictive. But after that the \texttt{build\_lp} method can now receive the URI which was stated on the command line as parameter. This means we can implement that method in the \ac{VM} handler we are writing.

The method, again, takes a number of parameters as input. The ones important for the \ac{VM} handler are:

\begin{itemize}
    \item \texttt{ctxt}

    Same as in \texttt{launch\_workflow}. It contains authorization tokens and metadata about the user and request.

    \item \texttt{image\_id}

    That ID references the language pack image entry which was generated by the \ac{API}. We need this ID for storing a reference to the result image of our language pack build.

    \item \texttt{source\_info}

    The \texttt{source\_info} parameter is a dictionary which contains all information about the source. In our case this would only be the URI.

    \item \texttt{lp\_params}

    Allows access to special parameters of the language pack. We do not need them at this point, but it could be used to set additional parameters for this language pack later.
\end{itemize}

The implementation of the method itself is easy now. We only need to extract the ID from the URI, check whether it exists and store the reference. The full implementation up to this point is shown in Listing~\ref{lst:worker-service-handler-buildlp}.

\lstinputlisting[float,floatplacement=H,language=Python,caption={Implementation of the \texttt{build\_lp} method. First the state of the language pack is set to \texttt{BUILDING}. Then the image ID is extracted from the \texttt{source\_info}. An utilty function checks wether the Glance image exists and the ID is saved. After that the state is set to \texttt{READY}. If an unexpected URI is received or an error occurs the state is set to \texttt{ERROR}.}, label=lst:worker-service-handler-buildlp]{listings/solum-service-worker-buildlp.py}

Another missing piece of information is the flavor which should be used for deploying and building. This defines how many resources the \ac{VM} will have access to. There are multiple options where this information could come from:

\begin{enumerate}
    \item \textbf{The language pack:}
    One could attach the information to the language pack and then build and deploy applications according to this setting. However a language pack can be used by totally different applications. Some might not need a lot of resources, some might do.
    \item \textbf{The app definition:}
    Attaching the information to the application definition allows finer control for applications with different requirements. However, even a single application can have different requirements. For example, a development setup might run fine with a lot less resources than a public production setup.
    \item \textbf{A deployment/build parameter:}
    The most flexible solution would be a parameter which is passed to all deploy and build commands. However, passing the information for each single invocation would be a bit annoying. A reasonable default would be useful at some point.
    \item \textbf{worker/deployer service configuration:}
    A simple configuration option in the worker and deployer service would be very simple to implement. However it would then deploy and build all applications with the same resources.
\end{enumerate}

Unfortunately none of these solutions alone is really good. It would probably make sense to combine a global, reasonable default with the ability to override it per application or better per deployment. However, since we need some kind of default anyway we will start with a configuration option only for now. Other options can be added later.

We are now able to retrieve all the information we need for building during the \texttt{launch\_workflow} invocation. We receive the basic information over the parameters and can query the database to get the base image which we stored for the language pack. With this information we can start the implementation of the worker interface.

\paragraph{Transfer source code into \ac{VM} instance:}
The first step for making the source code available in a \ac{VM} is to download the code. Since Solum currently only supports Git and since Git definitely is a very popular \ac{SCM} system we will focus on using Git for now. However, exchanging the code downloading logic should be relatively easy since it is mostly independent of the other logic. Cloning the Git repository of course only needs to happen while building. This means we only need to execute it if we received \texttt{build} as a command from the \texttt{workflow} parameter. For cloning the Git repository we can invoke the \texttt{git} command line program. In order to prevent name clashes with already existing folders we use the Python standard library for creating a temporary directory (\texttt{tempfile.mkdtemp()}). After that we can invoke \texttt{git clone <url> <tmpdir>} for cloning the Git into the temporary directory. Now we have the source code for further processing.

The next step is to launch a \ac{VM} instance and upload the code into it. We already decided that we will use Packer for this. It can handle the launching and uploading of images. However, we still need to pass a lot of information to it. First Packer needs to be able to authenticate itself for being able to launch new instances on Glance. Usually the \texttt{ctxt} parameter contains authentication keys for allowing services to execute actions within the context of the invoking user. Unfortunately, we cannot rely on this information at this point. This is because we also need to take automatically triggered builds into account. Solum supports launching builds after a webhook is triggered (this is usually used to trigger a new build as soon a Git push occurs). In this case \texttt{ctxt} will not contain such authentication tokens since no OpenStack user triggered the build. Luckily this problem is already solved in the existing Solum code for deploying images using HEAT during the deploy step. The Solum team decided to solve it by storing credentials which allow such authentications in the database during the application creation. So we can use those for this task. Otherwise Packer also requires information about which image should be started and how many resources should be allocated for it. Since we stored the Glance ID in the language pack and the flavor as a setting this information is easy to acquire. During earlier planning we did not consider a floating IP pool. This pool holds IPs which are accessible to the public network. We need such an IP for Packer being able to \ac{SSH} to an instance. So for now we will pass this information in via a configuration option too.

As it turns out this is all the information we need for starting a Packer build. So the next step is to write all this information into a temporary Packer \ac{JSON} file and invoke Packer with this file. Since the Python standard library has support for \ac{JSON} this is a simple task. With this file we are now able to invoke \texttt{packer build <file>} and be done. The code so far could look like in Listing~\ref{lst:worker-service-handler-packerbuild-skeleton}.

\lstinputlisting[float,floatplacement=H,language=Python,caption={Basic implementation skeleton of a Packer invocation. The Packer build file is first build as a Python dictionary and then serialized to a temporary \ac{JSON} file via \texttt{json.dumps}. Then Packer is invoked using that file.}, label=lst:worker-service-handler-packerbuild-skeleton]{listings/solum-service-worker-packerbuild-skeleton.py}

This, however, only is a basic skeleton. The Packer build does not do anything. So we will need to add an upload provison step:

\begin{lstlisting}[caption=The upload provisioner of the Packer file]
provisioners": [
    {
        "type": "file",
        "source": git_clone_dir,
        "destination": "/tmp/source/",
        "direction": "upload",
    },
],
\end{lstlisting}

Here it is important that the \texttt{git\_clone\_dir} does not contain a trailing slash but \texttt{destination} does. Otherwise the temporary directory would be uploaded to \texttt{/tmp/source/tmpXXXXX/} and not directly into the \texttt{/tmp/source/} folder.

After this we are able to start a new \ac{VM} instance based on the Glance image referenced in the language pack and upload code to it. Packer will automatically power the machine off after that and saves a snapshot to Glance.

Summarized we are now able to to enrich arbitrary images from Glance with source code and store them back to Glance.

\paragraph{Run build commands:}
The next step is to execute a series of commands after the source is uploaded. Since we so far are not able to associate further metadata with a language pack we will rely on the code coming with a \texttt{build.sh} script which builds the software. The only change required for this is to execute this script as part of the Packer build. This can be done with a small addition to the provisioner section:

\begin{lstlisting}[caption=Execution provisioner section. This changes the current directory to the source directory and executes the \texttt{build.sh} within that directory.]
{
    "type": "shell",
    "inline": [
        "cd /tmp/source/",
        "./build.sh",
    ],
},
\end{lstlisting}

The script can run arbitrary commands within the \ac{VM} and the result image is saved automatically by Packer. However, if the build script fails we have no chance to figure out why it did. Packer deletes the temporary \ac{VM} without backing it up after a failed build. So it would not even be possible to log into a file within the \ac{VM}. Of course Packer is logging the execution output to its standard output. This output, however would only available to the administrators of the machine which is running the Solum services. The user of Solum does not necessarily has the privileges to acquire this information. Fortunately, Solum already solved this issue for the Docker based builds. The logs are written to a temporary file and can then be uploaded to Swift. There they can also be accessed by the invoking user.

For debugging purposes we additionally want to log into the default Solum log. Since this logging mechanism works using the \texttt{oslo.log} library we need to redirect the output of Packer into, for example, \texttt{LOG.info} calls. The Python function \texttt{subprocess.check\_output}\footnote{\url{https://docs.python.org/2/library/subprocess.html\#subprocess.check_output}} allows to execute external programs while recording their output into a string. Unfortunately this method is not viable in this use case due to two reasons. First it captures the whole output at once. This means the function blocks until the program terminates. So it is only possible to log the entire output after the program is finished. Watching the execution live is not possible then. The more important reason is, that the function raises an exception if the command exits with a non zero exit code. While this might sound desirable it actually makes the function entirely useless for this use case. If the Packer build fails, the process returns a non zero exit code. This means the exception is raised. The exception of course then terminates the further execution, including the returning of the process output. So we have no way to get the output which contains the reason for the non zero exit code. This means that we have to implement our own output reading functionality. Since we likely want to iterate over each line it makes sense to implement this functionality as a generator function. Listing~\ref{lst:process-output-generator} shows a possible implementation. The \texttt{poll()} method checks whether the process exited and returns the exit code if it did. As long as the process did not finish we read the next line from the output. As soon a line is received it is returned via yield. After the program exited the exit code is saved and the potentially remaining lines are yielded. In the end the exit code is checked and an exception is raised unless it is zero. A generator like this is easy to consume using a loop like this: \texttt{for line in \_process\_output(cmdline)}.

\lstinputlisting[float,language=Python,caption={A generator method for iterating over lines of a process output.}, label=lst:process-output-generator]{listings/process-output-generator.py}

The remaining implementation is trivial. It, however, took a bit of refactoring in order to be able to reuse parts of the Docker image building logic. A lot of the smaller utility functions were only available in the local Docker handler scope.  Those had to be turned into static functions and moved into utility files at places which are reasonable to import by the \ac{VM} handler. But after that it was possible to reuse those functions and the building code remained relatively simple.

\paragraph{Save resulting state:}
We now have a working build and Packer automatically stores the image in Glance. So there really should not be a lot additional work to do in order to save the result state. However we still need to associate the result Glance image ID with the assembly. This is required since the deployer needs to know what to deploy. Packer is outputting the relevant ID as part of the output. This line looks like:

\begin{lstlisting}
--> openstack: An image was created: 62a78a03-c6bb-47eb-be31-5fb7156f48bb
\end{lstlisting}

Parsing this is certainly possible. But since Packer outputs a lot more information during the build it would be hard to create a fault tolerant solution which cannot be confused by potential similar lines outputted by a build script of an application. Fortunately Packer can also output a machine friendly log format. A full documentation about this format can be found at \cite{packer_machine_readable_doc}. For enabling this machine readable format the command line parameter \texttt{-machine-readable} needs to be added to the Packer invocation. After that the line above is logged as:
\begin{lstlisting}
1474736492,,ui,say,--> openstack: An image was created: 62a78a03-c6bb-47eb-be31-5fb7156f48bb
\end{lstlisting}

The first number is a simple timestamp. The second empty entry is can be a specific \texttt{target}. The next value is the \texttt{type} followed by an arbitrary amount of \texttt{data} values. This output however does not make parsing the Glance ID from the given log line easier at all. It only adds a bit of information before the actual line. But the \texttt{-machine-readable} option does not only add this information, but also logs some extra lines which are usually suppressed. One of those lines is the logging of the actual artifact which got created during the build. This line looks like:

\begin{lstlisting}
1474736492,openstack,artifact,0,id, 62a78a03-c6bb-47eb-be31-5fb7156f48bb
\end{lstlisting}

Parsing this now is easy. It only is a matter of splitting a the \texttt{,} symbol and check for the values \texttt{openstack}, \texttt{artifact} and \texttt{id}. This ID can then be passed to the deployer if a \texttt{deploy} command was issued in the \texttt{workflow}.

\paragraph{Implementation of deployer}
\label{par:Implementation of deployer}
As described earlier Solum has a special service for handling the deployment of instances. While this might sound like a simple call to Glance to issue the launch of the instance, this turned out a lot more complex. Since Solum automatically opens ports to instances and supports scaling\footnote{While scaling was possible via the \ac{API} at the time this thesis was written, actual scaling never happened even in the existing Docker handler. Thus scaling is also not implemented as part of this thesis.} of applications  HEAT is used for easier deployment and updating. The general process is that a \ac{HOT} file is generated which is then launched as new HEAT stack. This however still does not explain that the deployer handler implementation has around 800 lines while the worker handler only has about 250 lines. There are two main reasons for this complexity. The first is that the handler is supporting different deployment methods for Docker containers. One is to launch it using a Nova instance which was configured with a Docker driver. The other is to deploy a CoreOS\footnote{\url{https://coreos.com/}} \ac{VM} instance which internally hosts the actual application as a Docker container. This leads to many \texttt{if} - \texttt{else} branches in the code. The second reason is that the deployer checks the state of the launched instance. It does this by sending HTTP requests to each opened port of the instance. If any of those HTTP requests is successful the application is marked as properly launched. If no HTTP request succeeded after a configurable amount of tries, the application launch is marked as failed.

Since we need to implement the same interface as the Docker deployer it makes sense to also use a HEAT based deploy for making tasks like scaling and port opening a little easier. This also means that we could share a lot of the existing logic. However this logic unfortunately was all implemented privately inside of the Docker deployer class. So there were a lot of methods which needed to be made static and moved to a shared place. While this was not too complex to do, it needed to be done carefully in order not to break the existing code. Since this code grew historically this took a lot of time. But it was possible to create a base HEAT handler and let the Docker and our new \ac{VM} handler derive from that one. Other utility methods were able to be moved to a helper file and then imported by both handlers. This cleanup allowed to strip about 300 lines of code from the Docker deployer and made it possible to implement the new \ac{VM} handler in just above 100 lines. Since the image already exists in Glance we only need to create a new stack and launch it. The Docker deployer also needs to care about the storage location of the Docker image and requires extra complexity due to this. Due to the refactoring this however took more development time than developing the Packer build step.

After the new deployer was implemented it can be enabled in the configuration by setting \texttt{handler = vm} in the \texttt{[deployer]} section. Additionally we need to implement the \texttt{deploy} step in the worker handler. Here we check whether the \texttt{deploy} task is issued in the \texttt{workflow} parameter. If that is the case we send a deploy request to the deployer \ac{API}. This is only a matter of passing the right parameters ahead. Listing~\ref{lst:worker-deploy-forwarding} shows the forwarding. The code forwards the \texttt{ctxt}, \texttt{assembly\_id}, \texttt{image\_loc} \texttt{image\_name}
and the \texttt{ports} variables to the deployer. \texttt{image\_loc} is empty (\texttt{None}) at this point since we only support deployment from Glance images anyway. The \texttt{image\_name} will be the ID to the generated Glance image.

At this point we are ready to submit this change to the Solum upstream repository.

\begin{lstlisting}[float,caption={Deploy invocation using the deployer \ac{API}.}, label=lst:worker-deploy-forwarding]
deployer_api.API(context=ctxt).deploy(
        assembly_id=assembly_id,
        image_loc=du_image_loc,
        image_name=du_image_name,
        ports=ports)
\end{lstlisting}


\paragraph{Submitting of the code to the Solum upstream repository}
\label{par:Submitting of the code to the Solum upstream repository}
After developing a worker and deployer handler for building and deploying \ac{VM} images, we now have a self contained set of changes which we can contribute to the Solum repository. In the beginning of Section~\ref{sec:Development} the submission process was already shortly mentioned. Following the usual Git development workflow we developed our changes in a local branch. Since we had to do a lot of refactoring, especially on the deployer, the changeset turned out rather big. Fortunately Git does allow to only stage parts of the pending changes into a commit. This allows to create commits which do the refactoring and add the actual building logic and documentation as a second commit. This makes it a lot easier for a reviewer to differentiate between refactoring changes and new code. After we split the changes into smaller chunks we can open a review on Gerrit. For this to work the commits need to get an unique change ID. This ID is used for tracking updates to the changeset. Since Git hash values change as soon the commit is changed or rebased, the hash alone does not help here. Fortunately the \texttt{git-review} tool placed some Git hooks in our local repository during the setup. These hooks are run after each commit and generate such ids. This means that after creating a commit, the commit message is extended to also contain the said ID. We can see this when running \texttt{git log} after creating a commit:

\begin{lstlisting}
$ git log
commit 475c5dc723e640c991a37507fc3bc8d831396d2f
Author: Erik Schilling <ablu.erikschilling@gmail.com>
Date:   Wed Sep 14 18:23:32 2016 +0200

    WIP: Build VM images using Packer

    Change-Id: I8be1544485956654375df714f707333789118f34
    Closes-Bug: #1582375
\end{lstlisting}

The \texttt{Change-Id} here is the ID for Gerrit.

Opening a new review request usually happens by manually pushing the changes to a Gerrit specific remote branch. Gerrit then automatically opens a review for the changes pushed to this branch. The command line to push to this Gerrit branch is pretty long and hard to remember. Fortunately \texttt{git-review} also saves this burden from the developer. Running \texttt{git-review} without any parameters is enough to open a review request for the changes which are pending in the currently checked-out branch. The tool automatically triggers a rebase of the local changes on top of potential, new remote changes and then creates review requests for each commit. The URLs for the review in the web frontend are then outputted to the terminal and can be used for reviewing. This workflow also works for updating already existing review requests.

For the changes we did this results in the following review requests:

\begin{itemize}
    \item \url{https://review.openstack.org/#/c/382828/}
    \item \url{https://review.openstack.org/#/c/370285/}
    \item \url{https://review.openstack.org/#/c/336570/}
\end{itemize}

Since \ac{WIP} patches during the development also got pushed to those review requests, it is possible to track the development of the changes there. For demonstrating the new functionality a video has been created\footnote{\url{https://www.youtube.com/watch?v=r72-TYvCdpQ}}. A shortened version of this video will be presented during the Solum fishbowl session at the OpenStack Barcelona summit\footnote{\url{https://www.openstack.org/summit/barcelona-2016/summit-schedule/events/16988/solum-application-platform-as-a-service-apaas-for-openstack}}.
