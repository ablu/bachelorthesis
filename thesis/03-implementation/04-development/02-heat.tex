\subsection{HEAT plugin for orchestration}

    For the prototype of the plugin (Listing~\ref{lst:heat-example-plugin}) we only returned a string constant and did not use any \ac{API} invocations. Adding this and accessing the Solum \ac{API} would be simple since the Python library provides a \texttt{Client} class which can be consumed for this. One option would be to implement the construction of this class directly in the resource plugin file and use it there. However HEAT also has a plugin structure for \ac{API} clients. This decouples the \ac{API} client construction from the resource plugin and allows multiple resource plugins to share the same clients. Especially since it is desirable to contribute the plugin to the collection of standard HEAT plugins we should implement the Solum client as a separate client plugin.

Unfortunately, there is no documentation for developing such client plugins. But it is possible to derive a Solum plugin from other standard client plugins which are located in the HEAT repository at \cite{heat_client_plugin_examples}. The \texttt{SenlinClientPlugin} at \cite{senlin_client_plugin} is a plugin which can serve as a base here. Implementing the plugin mainly consists of setting a plugin name and implementing a \texttt{\_create()} method which constructs the new \ac{API} client instance. A few other methods are required for the handling of exceptions raised by the plugin. Listing~\ref{lst:heat_solum_client} shows the implementation of the core functionality of the plugin. The \texttt{self.context} variable contains the necessary information for authenticating on the Solum \ac{API} endpoint. Otherwise the method only returns the result of the Solum library \texttt{Client} constructor. The methods for exception handling are left out here for brevity. Those methods mostly check whether exceptions which occurred were thrown by the plugin.

\lstinputlisting[caption={Implementation of the Solum Client plugin}, label=lst:heat_solum_client,float,language=Python]{listings/heat-solum-client-plugin.py}

This client plugin can now be accessed using the \texttt{self.client()} method from within the resource plugin. For letting HEAT know which client is desired, it is required to set \texttt{default\_client\_name} to the key with which the client plugin was registered. In our case this would be \texttt{'solum'}. The method then returns the Solum Client object which was initalized and returned by the \texttt{\_create()} method of the plugin. This object is part of the Solum library, but also is not documented currently. However, we can read the code of the Solum command line application for getting examples of the usage.

For building the application we need meta data like the language pack and the Git source repository. Since this information is already acquired when registering a new application it makes sense to use an app name as input for our plugin. Triggering a new build can then happen by creating a new workflow for this app which issues the \texttt{build} command. Figure~\ref{fig:heat-plugin-architecture} outlines the invocation workflow of the build process.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.4]{figures/solum_heat_plugin_architecture.png}
  \caption{Outline of the HEAT plugin execution workflow.}
  \label{fig:heat-plugin-architecture}
\end{figure}

An issue which was already shortly mentioned in Section~\ref{sub:HEAT plugin for orchestration} is that the workflow \ac{API} return values do not allow to retrieve the build results of the build step. The use case to only specify a \texttt{build} command with a \texttt{deploy} never was relevant for Solum so far. Internally the worker has the reference to the build results for passing it to the deployer, but the information is not accessible to the invoker of the \ac{API}. The solution is to store the information in the database in a way that it can be retrieved with the information which is returned by the \ac{API}. This is already done for the language pack building, for example. Here we need to know the base image which was generated during the language pack build for building an application. For language packs this is solved by saving the ID of the generated image into the \texttt{Image} table. Fortunately this table also holds an entry for each triggered application build. In the previous section the \texttt{build\_id} parameter of the \texttt{launch\_workflow} method was explained already. This parameter is a reference to a row in the table. The build step currently only uses this ID for updating the status of the build. But since the same logic was already required for storing the language pack we can reuse this logic. In our case we only need to invoke a method of the conductor \ac{API} after the build succeeded and pass the generated image reference to it:

\begin{lstlisting}
conductor_api.API(context=ctxt).update_image(
        build_id, IMAGE_STATES.READY,
        du_image_name, None)
\end{lstlisting}

This marks the image as \texttt{READY} and saves \texttt{du\_image\_name} in the \texttt{external\_ref} column of the \texttt{Image} table. However, we still need to be able to retrieve this information based on the information returnd by the workflow creation. For this we will need to take a look at the Solum database schema which is illustrated in Figure~\ref{fig:solum_image_db_schema}. The \texttt{Workflow} table has a reference to the \texttt{Assembly} table using the \texttt{assembly\_id}. From there \texttt{Image} is reachable via the \texttt{image\_id} column. This means that the required information is reachable using only the workflow ID, which is returned by the workflow creation. However, there is no \ac{API} access to objects of the \texttt{Assembly} and \texttt{Image} table. Those tables are only used internally and are not meant to appear in the \ac{API}. So we will need to implement new functionality for exposing this information. In order to evaluate the ideal implementation we will first need to look at the context in which we will query this information.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.4]{figures/db_structure.png}
  \caption{The database schema relevant for retrieving the \texttt{external\_ref} field.}
  \label{fig:solum_image_db_schema}
\end{figure}

For triggering a build we initially need to launch a new workflow. After creating a new workflow, the \ac{API} will call the \texttt{launch\_workflow} method of the worker service and return the newly created workflow object. With this object we can access the ID of the workflow as identification for further \ac{API} calls. The object, at this point, cannot contain any information about the result image since the worker service receives the \texttt{launch\_workflow} command asynchronously and might not even have started building at the time we receive the reply from the creation command. This means that we will have to repetitively poll the \ac{API} for getting an update on the build process. This has to be done by requesting the workflow information in a loop and checking for the \texttt{status} property. Once this property has the value \texttt{'BUILT'} the \texttt{external\_ref} field was set in the database. So it makes sense to attach the \texttt{external\_ref} property to the workflow object which we are already querying for doing the status check. Then we would not need an additional \ac{API} call for acquiring it.

For adding this property we need to extend the \texttt{get()} method in the \texttt{workflow\_handler.py} file of the Solum \ac{API}. Since the assembly table is already queried here for getting the build status, we only need to add a query to the \texttt{Image} table. The code for this is shown in Listing~\ref{lst:solum-imageid-enrich}.

\lstinputlisting[caption={Enriching of the workflow object with the image \texttt{external\_ref} property},float,label=lst:solum-imageid-enrich,language=Python]{listings/solum-imageid-enrich.py}

After this, there still is a small change required to the \ac{API} datamodel. This model is used for serializing the objects returned by the \texttt{workflow\_handler.py} to \ac{JSON} or \ac{XML} (depending on the client). Since we added a new property to the workflow object, we also need to register it in the \texttt{Workflow} model. This is done by appending a simple line to the class body:

\begin{lstlisting}
external_ref = wtypes.text
\end{lstlisting}

This should make the \texttt{external\_ref} property to show up in the results of the \ac{API} \texttt{get} call. But during testing it turned out that the \texttt{Image} table entry is created, but the reference to it was never stored in the \texttt{Assembly} table entry. This of course breaks the reference chain required for accessing the field. Fortunately it was only required to reorder the insert process of the image and assembly for being able to also save the image ID to the \texttt{Assembly} table. With the reference chain being fixed the solution now works.

After the \texttt{external\_ref} is now stored to the database and is made accessible using the \ac{API} we can implement the HEAT resource plugin. Since we decided to use an app name for retrieving the meta data, we need to create a resource property for this. The produced output of the resource will be the generated \texttt{external\_ref}. So we will define this as an attribute which can be accessed by other resources. Listing~\ref{lst:heat-plugin-attributes} shows those declarations.

\begin{lstlisting}[caption={The property and attribute declaration of the resource},float,label=lst:heat-plugin-attributes,language=Python]
properties_schema = {
    APP_NAME: properties.Schema(
        properties.Schema.STRING,
        _('Name of the Solum app.')
    ),
}
attributes_schema = {
    EXTERNAL_REF: attributes.Schema(
        _('Reference to the external'),
        cache_mode=attributes.Schema.CACHE_NONE,
        type=attributes.Schema.STRING
    ),
}
\end{lstlisting}

The \texttt{handle\_create} method now needs to query the Solum app ID which corresponds to the app name, launch a new build workflow and wait for the result. A simplified implementation of this is shown in Listing~\ref{lst:heat-plugin-simplified-create-method} (failure handling is left out here for brevity).

\lstinputlisting[caption={A simplified version of the implementation of the \texttt{handle\_create} of the HEAT resource plugin.},float,label=lst:heat-plugin-simplified-create-method,language=Python]{listings/heat-plugin-simplified-create-method.py}

In order to prevent issues with hanging builds or manually stopped services there also should be a mechanism which can recover without manual maintenance. A simple solution for this is a timeout. However, choosing a timeout value which is reasonably short, but also satisfies the requirements of complex builds is not possible. It might be perfectly fine for a build to take multiple hours in one case, while the build time should never exceed a few minutes in other cases. So it makes sense to create a new property with which the user can control the timeout if the default value is not viable for his application. In this case a timeout of 30 minutes should suffice for the most simple applications. The implementation of the loop then looks like in Listing~\ref{lst:heat-plugin-wait-loop}.

Finally this change was submitted to the HEAT repository as a review request at \cite{heat_plugin_review_request}. The necessary changes to the Solum \ac{API} were submitted as \cite{solum_api_changes_for_heat}.

\lstinputlisting[caption={A simplified version of the implementation of the \texttt{handle\_create} of the HEAT resource plugin.},float,label=lst:heat-plugin-wait-loop,language=Python]{listings/heat-plugin-wait-loop.py}
