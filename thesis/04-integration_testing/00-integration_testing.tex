\chapter{Proof of Concept}
\label{chap:Proof of Concept}

In Section~\ref{sec:Example Use Cases} we defined a series of example use cases. In this chapter we will now proof that we can build the use cases with the developed solutions. But the first step is to install our changes in the testing environment.

\section{Installation on testbed}
\label{sec:Installation on testbed}
In Section~\ref{sec:Development environment} the setup of the development environment was described. This environment will now be reused for the integration tests.

Since Solum is a Python application and thus does not require any compilation the installation is relatively easy. The running services also do not require a lot of resources on their own (the \ac{VM} images which are being built will require resources separately). This allows an installation on the controller node next to the other OpenStack services. For the installation we first need to acquire the Solum source code with our changes. For getting the core services we can run \texttt{git clone \url{https://github.com/openstack/solum.git}}. This clones the repository which contains the api, worker, deployer and conductor services. After that we can checkout the changeset we submitted. For testing the second changeset of the review request with the change ID \texttt{I6038b717ef629b08d3c505930e4dd6ed3a2829bc}\footnote{\url{https://review.openstack.org/\#/c/385198/2}} was used. Checking out this revision is possible using the following command:

\begin{lstlisting}
git fetch https://git.openstack.org/openstack/solum refs/changes/98/385198/2 && git checkout FETCH_HEAD
\end{lstlisting}

Solum has all the required run dependencies listed inside of a \texttt{requirements.txt} file which is located in the repository root. To install those it is required to execute \texttt{pip install -r requirements.txt} within the clone directory. Installing Solum itself is possible by running \texttt{python ./setup.py install}. This will create scripts in \texttt{/usr/bin/} and copy and byte-compile the Python modules of Solum to the Python system library folder. Running \texttt{solum-api} can verify that the installation was successful. However, there still is a bit of configuration required. Listing~\ref{lst:solum-integration-testing-config} shows the minimal configuration entries which are required inside of the \texttt{/etc/solum/solum.conf} file.

For security reasons we should also run the Solum services within a restricted user account. Creating a new system account\footnote{A system account does not have a home directory and logging in with this user is disabled.} can be done using \texttt{useradd -{}-system solum}.

\begin{lstlisting}[caption={Test configuration file for Solum. This file needs to be located at \texttt{/etc/solum/solum.conf}}, label=lst:solum-integration-testing-config,escapechar=*,float]
[keystone_authtoken]
admin_user = admin
admin_password = <admin password>
admin_tenant_name = admin
auth_uri = http://fg-cn-decaf-head1.cs.upb.de:5000/v2.0
cafile =
auth_protocol = http
auth_port = 35357
auth_host = fg-cn-decaf-head1.cs.upb.de

[database]
connection = mysql+pymysql://solum:<db-password>@*\break*fg-cn-decaf-head1.cs.upb.de/solum?charset=utf8

[api]
image_format=vm

[worker]
handler=vm

[deployer]
handler=vm
\end{lstlisting}

For automatically starting the services at boot time and for easier log collection we should also create Systemd Units\footnote{\url{https://www.freedesktop.org/software/systemd/man/systemd.unit.html}} for the services. In Listing~\ref{lst:solum-systemd-unit} an example unit file is shown for the \ac{API} service. The unit files for the other Solum components can be derived from that example. Enabling the unit files for automatic startup at a reboot is done by \texttt{systemctl enable solum-api}. For starting the service without rebooting \texttt{systemctl start solum-api} can be used.

Our Solum installation is fully functional now. Since we will do the upcoming tests based on CentOS images, we will register a language pack for this. For this we first need to create a Glance image with a CentOS download URL. After the image is created we can register it using \texttt{solum lp create centos7 glance://<image id>}. After giving the worker a bit of time to process the request this image should show up with the state \texttt{READY} in the output of \texttt{solum lp list}.

In order to support deployments backed by HEAT we will also need to install the HEAT plugin. Since the plugin was submitted for submission to the official HEAT repository it is the easiest solution to install that relevant Git commit on the system. For this the HEAT Git repository\footnote{\url{https://github.com/openstack/heat}} has to be cloned. After switching to the repository clone (\texttt{cd heat}) we can checkout the commit from the Gerrit review request. This proof of concept was executed with the first changeset of the review request at \cite{heat_plugin_review_request}. Gerrit automatically generates command lines for checking out changesets which are in a review. In this case the following command can be used to checkout the state of the commit at the first changeset:

\begin{lstlisting}
git fetch https://git.openstack.org/openstack/heat refs/changes/76/387076/1 && git checkout FETCH_HEAD
\end{lstlisting}

Since we are going to run a newer version of HEAT compared to the one which was initially installed using Packstack, we will first have to stop the already installed HEAT services. This can be done with \texttt{systemctl stop openstack-heat-*}. To prevent that the new HEAT version uses outdated modules of the existing install or breaks existing components by upgrading libraries the new version should be installed in a separate virtual Python environment. Such an environment decouples the installation from the system-wide Python libraries. The following steps demonstrate how to create such a virtual environment, activate it and install the required dependencies of HEAT:

\begin{lstlisting}
virtualenv .venv/
source .venv/bin/activate
pip install -r requirements.txt
pip install pymysql # since our OpenStack install uses MySQL
\end{lstlisting}

The first line creates an empty environment which only contains Python itself. The second line activates the environment within the current Bash session. This means that all further commands which are executed during this session are run within the virtual environment. The next line installs the dependencies as listed in the \texttt{requirements.txt} file of the HEAT repository. This step might take a while. Finally it is also required to install the \texttt{pymysql} dependency. This dependency is not listed in the requirements.txt since HEAT supports multiple database backends. After that, it is possible to start the HEAT components. For the purpose of this proof of concept it is enough to run them without installing first. The HEAT repository contains ready to run launchers for the individual components. In our case we only need to execute \texttt{bin/heat-api} and \texttt{bin/heat-engine}. For keeping the services running even if the \ac{SSH} connection to the server is terminated, it makes sense to launch these services using a terminal multiplexer like \texttt{screen}\footnote{\url{https://www.gnu.org/software/screen/manual/screen.html}} or \texttt{tmux}\footnote{\url{https://tmux.github.io/}}.

After this we prepared the test environment with the necessary components to test the use cases defined earlier.


\section{Standalone application}

The most simple use case described was a standalone application. As an example WordPress was given. WordPress requires a PHP-enabled webserver and a database backend. In this case we will install both dependencies into the same image which will hold the WordPress installation. Since we will have to create a \texttt{build.sh} file inside of the repository we will have to copy the main repository to a private testing repository. Here we can simply create a fork of the GitHub mirror\footnote{\url{https://github.com/WordPress/WordPress}} and create that file there. The \texttt{build.sh} file should look like in Listing~\ref{lst:integration-test-wordpress-buildsh}.

The script first installs the dependencies. \texttt{httpd} is the Apache web server and \texttt{mariadb} is a MySQL compatible database server. Then the \texttt{httpd} and \texttt{mariadb} services are registered for being launched on system boot (\texttt{systemctl enable}). Since WordPress requires a database the next step is to create an empty MySQL database and an user with access to it. For this task the \texttt{mariadb} service has to be launched first.
The final step is to copy the content of the source code repository to the web server root directory (\texttt{/var/www/html/}). Since WordPress is written in PHP, no other preceding compilation steps are required.

WordPress however requires a configuration for running. It can automatically generate one on the first launch, but it requires write access to the web server directory then. For the purpose of demonstrating the proof of concept we will simply use a fixed configuration file at this point. This file needs to be created in the root of the Git repository fork. For this test a configuration file from \cite{wordpress_fork_config} is used.


\begin{lstlisting}[language=bash,float,caption={\texttt{build.sh} script for WordPress. It installs a HTTP and database server and installs WordPress onto those.}, label=lst:integration-test-wordpress-buildsh]
#!/bin/bash
sudo yum install -y httpd mariadb-server mariadb php php-mysql
sudo systemctl enable httpd
sudo systemctl enable mariadb

sudo systemctl start mariadb
echo "CREATE DATABASE wordpress" | sudo mysql
echo "GRANT ALL ON wordpress.* TO 'username'@'localhost' IDENTIFIED BY 'password'" | sudo mysql
echo "FLUSH PRIVILEGES" | sudo mysql

sudo cp -r * /var/www/html/
\end{lstlisting}

The \texttt{buld.sh} and \texttt{wp-config.php} file need to be added, commited and pushed to the Git repository fork. After that we are ready to create a WordPress application description file. This file should look like in Listing~\ref{lst:integration-test-wordpress-appfile}. The language pack points to the CentOS image we registered earlier and the git URL is pointing to our private fork. Since we want to expose the webserver in the final \ac{VM} instance we also define port 80 as open. Assuming that the filename of the app-file is \texttt{wordpress.yaml} we can register the application using:

\begin{lstlisting}
solum app create --app-file wordpress.yaml
\end{lstlisting}

This sends the information of the \texttt{wordpress.yaml} file to the Solum \ac{API} service which stores it in the database. After that we can trigger a deployment using:

\begin{lstlisting}
solum app deploy wordpress
\end{lstlisting}

This triggers the build process with the actions \texttt{build} and \texttt{deploy} (as specified in the app-file). Since this step clones the Git repository, launches \ac{VM} instances and saves snapshot it is very time consuming. For getting a better overview about the build time Figure \ref{fig:integration-test-wordpress} shows the time spent for each building step. The measuring method relies on evaluating the Systemd Journal timestamps of the Solum service logs. However, the test system was operating with full memory during the tests and occasionally had to swap data to the disk. A system with more available memory or faster disks might be able to run the individual steps significantly faster.

Apart from the build itself, the most time is spent on waiting for the image to start and waiting for the snapshot to be taken. The actual build time in this case mainly depends on the download time of the dependency packages. The provisioning of the database and the copying of files does not require a lot of time.

\begin{figure}
    \definecolor{gitClone}{HTML}{F0F0F0}
    \definecolor{launchServer}{HTML}{D9D9D9}
    \definecolor{waitForSSH}{HTML}{BDBDBD}
    \definecolor{uploadDone}{HTML}{969696}
    \definecolor{buildDone}{HTML}{737373}
    \definecolor{imageSaved}{HTML}{525252}
    \definecolor{deploy}{HTML}{252525}
    \centering

    \pgfplotstableread{ % Read the data into a table macro
    Label  gitClone  launchServer waitForSSH uploadDone buildDone  imageSaved deploy
    4      30        17           37         19         139        181        12
    3      30        15           38         18         141        198        12
    2      26        19           32         20         192        178        12
    1      29        34           54         18         163        200        15
    }\testdata

    \begin{tikzpicture}
    \begin{axis}[
                xbar stacked,   % Stacked horizontal bars
                legend style={
                legend columns=4,
                    at={(0.5,-0.4)},
                    anchor=north,
                    draw=none
                },
                xmin=0,         % Start x axis at 0
                ytick=data,     % Use as many tick labels as y coordinates
                yticklabels from table={\testdata}{Label},  % Get the labels from the Label column of the \datatable
                xtick={0,100,200,300,400,500,600},
                width=.9\textwidth,
                area legend,
                y=8mm,
                tick align = outside,
                x unit=s
    ]
    \addplot [fill=gitClone] table [x=gitClone, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=launchServer] table [x=launchServer, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=waitForSSH] table [x=waitForSSH, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=uploadDone] table [x=uploadDone, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=buildDone] table [x=buildDone, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=imageSaved] table [x=imageSaved, meta=Label,y expr=\coordindex] {\testdata};
    \addplot [fill=deploy] table [x=deploy, meta=Label,y expr=\coordindex] {\testdata};
    \legend{git clone, launch server, wait for SSH, upload code, build, save image, deploy}
    \end{axis}
    \end{tikzpicture}
    \caption{Break down of the time spent in different building states of four subsequent deployments of the WordPress application.}
    \label{fig:integration-test-wordpress}
\end{figure}


\begin{lstlisting}[language=yaml,caption={WordPress app file for Solum. The file defines the source location and the build workflow.}, label=lst:integration-test-wordpress-appfile,float]
version: 1
name: wordpress
description: WordPress
languagepack: centos7
source:
  repository: https://github.com/Ablu/WordPress.git
  revision: master
workflow_config:
  run_cmd: echo run
  test_cmd: echo test
trigger_actions:
- build
- deploy
ports:
- 80
\end{lstlisting}


\section{Composed application}
The second use case which was presented was the build and deployment of an application which consists of multiple composed parts. While there are many large applications which are designed like this (a prime example being OpenStack itself). It is hard to find an application which is not too complex to set up and thus suits as a good example for this proof of concept. Because of this we will use a project which was created for the examination of the lecture ``Verteilte Algorithmen und Datenstrukturen''\footnote{\url{http://cs.uni-paderborn.de/ti/lehre/veranstaltungen/ss-2016/verteilte-algorithmen-und-datenstrukturen/}}. The project implements a visualization of the SHARE algorithm as presented in the lecture material at \cite{vdua_share_algorithm}. The application suits well for this test since it consists of two parts. One part hosts the \ac{API} which was written in Java. The other part provides an user interface which was written in HTML and TypeScript. The communication between the two components happens using a websocket\footnote{\url{https://www.html5rocks.com/en/tutorials/websockets/basics/}} connection.

For composing the two components we will rely on our HEAT plugin and use a HOT file for building and deploying. The first step for this is to define the build of the two components. For this we will first need to register the components as Solum apps. Listing~\ref{lst:solum-share-appfile} shows the app file for the backend service. It uses the already introduced \texttt{centos7} language pack as a base and sets the Git repository address. The app file for the frontend looks mostly the same, but points to the frontend Git repository at \texttt{https://github.com/Ablu/share-implementation-frontend.git} and exposes the port 80 instead of the \ac{API} service port. Listing~\ref{lst:solum-share-ui-appfile} shows this file. Those app files have to be registered by running \texttt{solum app create -{}-app-file <app file>} for both files. After that we can start to create the HOT file.

\begin{lstlisting}[language=yaml,caption={Solum App file for the backend service.},float,label=lst:solum-share-appfile]
version: 1
name: share
description: A SHARE implementation
languagepack: centos7
source:
  repository: https://github.com/Ablu/share-implementation.git
  revision: master
workflow_config:
  run_cmd: echo run
  test_cmd: echo test
trigger_actions:
- build
- deploy
ports:
- 9456
\end{lstlisting}

\begin{lstlisting}[language=yaml,caption={Solum App file for the SHARE frontend.}, label=lst:solum-share-ui-appfile,float]
version: 1
name: share-ui
description: The UI for the SHARE implementation API
languagepack: centos7
source:
  repository: https://github.com/Ablu/share-implementation-frontend.git
  revision: master
workflow_config:
  run_cmd: echo run
  test_cmd: echo test
trigger_actions:
- build
- deploy
ports:
- 80
\end{lstlisting}

The first resources we need to define will be the Solum builds. Since our plugin registers as \texttt{OS::Solum::Build} resource we will define the builds for the two components as shown in Listing~\ref{lst:share-hot-build-resources}:

\begin{lstlisting}[language=yaml,caption={Build resources for the SHARE service backend and frontend.},float,label=lst:share-hot-build-resources]
api_image:
  type: OS::Solum::Build
  properties:
    app_name: share
frontend_image:
  type: OS::Solum::Build
  properties:
    app_name: share-ui
\end{lstlisting}

The \texttt{app\_name} specified here needs to match with the name of the respective app files. The HOT file will trigger builds for each app. It will only execute the \texttt{build} step of the workflow and expose the resulting Glance image reference using the \texttt{external\_ref} attribute. The next step is to define the Nova instances based upon this build results. Listing~\ref{lst:share-hot-compute-resources} shows the resource definitions required for that. For readability network and floating IP definitions are left out in that listing.

\begin{lstlisting}[language=yaml,caption={\texttt{OS::Nova::Server} resources which utilize the Solum builds defined in Listing~\ref{lst:share-hot-build-resources}.},float,label=lst:share-hot-compute-resources]
api_instance:
  type: OS::Nova::Server
  properties:
    image: { get_attr: [api_image, external_ref] }
    flavor: m1.small
frontend_instance:
  type: OS::Nova::Server
  properties:
    image: { get_attr: [frontend_image, external_ref] }
    flavor: m1.small
\end{lstlisting}

While this is enough for launching instances of the two services, those two instances do not know anything about each other. In this case the frontend service requires to know how to access the \ac{API} service. So we will need to somehow pass the \ac{API} address to the frontend instance during the stack build. The code of the frontend reads the address from the file \texttt{/usr/share/nginx/html/app/config.js} and tries to establish a websocket connection to that location. This means that we need a way to set the configuration value in the launched instance. Doing this at build time is not really feasible. For example, it might be required to restructure the deployment to contain multiple \ac{API} services which are load balanced. Then it would be necessary to rebuild the frontend services, only because the \ac{API} service IP changed. Thus doing the change only in the launched instance provides a lot more flexibility regarding stack updates. This is possible by running a replacement command over the content of the configuration file at boot time. Running commands at boot time is possible by defining the \texttt{user\_data} property of the \texttt{OS::Nova::Server} resource. This property must be a string which defines a script. This script is then executed at boot time. For this to work, the launched image needs to have the \texttt{cloud-init}\footnote{\url{https://cloudinit.readthedocs.io/en/latest/}} service installed. However this is no problem, since all major Linux distributions have this service installed inside of their cloud releases. The CentOS image we are using is no exception here.
Listing~\ref{lst:share-hot-user-data} shows how the \texttt{user\_data} can be utilized for setting the \ac{API} service IP in the \texttt{frontend\_instance}. Since we did not hard code the IP address in the HOT file we need to use the \texttt{str\_replace}\footnote{\url{http://docs.openstack.org/developer/heat/template_guide/hot_spec.html\#str-replace}} functionality for embedding the floating IP address of the \ac{API} into the script we execute. The script then replaces the default value of the configuration (\texttt{location.hostname}) with the IP of the \ac{API} service.


\begin{lstlisting}[language=yaml,caption={Definition of the \texttt{user\_data} property for the \texttt{frontend\_instance}.},float,label=lst:share-hot-user-data]
user_data:
  str_replace:
    template: |
      #!/usr/bin/env bash
      sed -i "s/location\.hostname/'%api_host%'/g" /usr/share/nginx/html/app/config.js
    params:
      "%api_host%": { get_attr: [ api_ip, floating_ip_address ] }
\end{lstlisting}


Finally a few boilerplate resources are required for setting up the networking and the floating IP addresses. The entire HOT file is shown in Listing~\ref{lst:share-hot-complete} for completeness. Assuming the file is saved as \texttt{share.yaml} it is now possible to launch it using \texttt{openstack stack create -{}-template share.yaml share}. This will create a new stack with the name \texttt{share} according to the template file.

Unsurprisingly the build time for the individual components is comparable to the build time of a single instance. However, it is important to note that one Solum worker service only executes a single request at a time. This obviously becomes a bottleneck when building multiple images at the same time. The HEAT engine builds the resources asynchronously. This means that all the resources, which have no dependency on pending resources, are built in parallel. Since the worker service only accepts a single build, the second build is waiting in the OpenStack message queue. Fortunately it is possible to execute multiple worker services in parallel. Each service waits for messages on the queue and blocks once it receives a message. This means that each worker only takes a single build command from the queue at a time. In our case this allows to build both resources in parallel by running two Solum worker processes. Otherwise there is little information gained by measuring the stack creation time breakdown. As expected the build phase consumes most of the time. Creating the floating IPs is fast and happens in parallel anyway. Finally, launching of the service instances again takes a bit of time.

After the stack build is completed it is also possible to benefit from the HEAT updating mechanism. For example, it is possible to add an \ac{SSH} key to the final instances for debugging purposes. This is done by adding \texttt{key\_name: <key>} as a property to a Nova server resource. Now it is possible to run \texttt{openstack stack update -{}-template <template file> <stack name>} to update the existing stack. HEAT will recognize that only the Nova server resources were modified but all other resources were left untouched. This will result in only the Nova server being rebuilt and the Solum build results being reused. This way it is also possible to add more server instances to the stack without waiting for the build time again.


\section{Combining generated images with existing ones}
Another use case which was defined initially was the combination of generated and existing images. The use case was defined before the decision was made to implement the orchestration abilities as a HEAT plugin. As shown in the previous user case test it is possible to define arbitrary stack templates and launch them. This means that the previous use case actually was a more complex use case than this one. For testing this use case it is only required to replace one of the Solum build resources from the previous test with a predefined image from Glance. This functionality is no feature of the developed plugin but of HEAT itself. Thus it will not be covered in further detail here.
